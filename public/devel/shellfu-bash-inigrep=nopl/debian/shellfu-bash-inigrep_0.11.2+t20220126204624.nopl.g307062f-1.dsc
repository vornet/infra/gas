Format: 3.0 (quilt)
Source: shellfu-bash-inigrep
Binary: shellfu-bash-inigrep
Architecture: all
Version: 0.11.2+t20220126204624.nopl.g307062f-1
Maintainer: Alois Mahdal <netvor+inigrep@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-inigrep
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-inigrep deb misc extra arch=all
Checksums-Sha1:
 7c9b98bdcc45f154fc8e83b7cb75ff7e667e1e4b 78081 shellfu-bash-inigrep_0.11.2+t20220126204624.nopl.g307062f.orig.tar.gz
 48ac2eb27ae4b215468bf3136ec4da06fd07bc5f 944 shellfu-bash-inigrep_0.11.2+t20220126204624.nopl.g307062f-1.debian.tar.xz
Checksums-Sha256:
 bc349bdec41eaffa47a7dc29785dfc4a76b3bfb78836b52c690111dae7ef6336 78081 shellfu-bash-inigrep_0.11.2+t20220126204624.nopl.g307062f.orig.tar.gz
 ce520a667623e365aad07ad564800d76cc4da39b13c7df8299502512c2d8090f 944 shellfu-bash-inigrep_0.11.2+t20220126204624.nopl.g307062f-1.debian.tar.xz
Files:
 1173548792c4ea670526476be0f80d2d 78081 shellfu-bash-inigrep_0.11.2+t20220126204624.nopl.g307062f.orig.tar.gz
 b56ee48709e3c1de878197dbb68d2819 944 shellfu-bash-inigrep_0.11.2+t20220126204624.nopl.g307062f-1.debian.tar.xz
