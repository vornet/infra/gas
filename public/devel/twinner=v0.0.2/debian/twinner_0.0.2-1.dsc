Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 a3ed6e094031dd09e87d0ab7d913d3b6c2567194 31820 twinner_0.0.2.orig.tar.gz
 923ee945f99fcb4c07ebca5468dc7accf9f214b7 792 twinner_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 b5aec5c712c94246a5b73561109f1654bae5117320fb8945284a1597796c26d4 31820 twinner_0.0.2.orig.tar.gz
 de9005bad4d56996f3744fd8633a9a968b20a4acd5611a206af967b624150a44 792 twinner_0.0.2-1.debian.tar.xz
Files:
 24c9c47bef1bd8fc3b36b1feda7dac2a 31820 twinner_0.0.2.orig.tar.gz
 3747d84bfcdc57a9b1681081fa1db6d2 792 twinner_0.0.2-1.debian.tar.xz
