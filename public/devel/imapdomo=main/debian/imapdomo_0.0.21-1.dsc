Format: 3.0 (quilt)
Source: imapdomo
Binary: imapdomo
Architecture: all
Version: 0.0.21-1
Maintainer: Alois Mahdal <netvor+imapdomo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/hacktop/imapdomo
Build-Depends: debhelper (>= 9), make
Package-List:
 imapdomo deb misc extra arch=all
Checksums-Sha1:
 5267a13de25e3b44993163ace266e81f705f0bba 42269 imapdomo_0.0.21.orig.tar.gz
 24ef6a68a8878b792f85accf65ddcdd12bbd02ea 932 imapdomo_0.0.21-1.debian.tar.xz
Checksums-Sha256:
 6cb530e16377b72ca64a6c5b690d81d4c79cda391f37b095fed1b6b1808d05aa 42269 imapdomo_0.0.21.orig.tar.gz
 43e2b9208903f1db34ed1034dc71da08aa4573f696acda9cd1169d4a422c2381 932 imapdomo_0.0.21-1.debian.tar.xz
Files:
 a5a86c19e315f67946e7da683148bfea 42269 imapdomo_0.0.21.orig.tar.gz
 f386c20b07ffb691b9fcf171694caa26 932 imapdomo_0.0.21-1.debian.tar.xz
