Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.10+t20201208112707.seeerr.gb516ead-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 585fce73060d698dd767515c5a7cfa94ba250a15 45228 shellfu-bash-xcase_0.0.10+t20201208112707.seeerr.gb516ead.orig.tar.gz
 62419e40cad180f0db25e532ba76231aab8d2217 1032 shellfu-bash-xcase_0.0.10+t20201208112707.seeerr.gb516ead-1.debian.tar.xz
Checksums-Sha256:
 fc5e414876ad9334c2d5e27e158ae744f413461125ac81b6fde2687777e7448c 45228 shellfu-bash-xcase_0.0.10+t20201208112707.seeerr.gb516ead.orig.tar.gz
 f2aecf2b0b01a0bc08970263f5e1d980c2be373b9fdcbadc0ef87cc7c94aeb1a 1032 shellfu-bash-xcase_0.0.10+t20201208112707.seeerr.gb516ead-1.debian.tar.xz
Files:
 483319488611fb85dd4ded2328492880 45228 shellfu-bash-xcase_0.0.10+t20201208112707.seeerr.gb516ead.orig.tar.gz
 e25519718562f747656dba45ebc705b1 1032 shellfu-bash-xcase_0.0.10+t20201208112707.seeerr.gb516ead-1.debian.tar.xz
