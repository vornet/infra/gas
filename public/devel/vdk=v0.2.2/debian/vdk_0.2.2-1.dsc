Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-common, vdk-pylib
Architecture: all
Version: 0.2.2-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 f238e2da676b1a48216283de4933f69f62abdaed 87995 vdk_0.2.2.orig.tar.gz
 ff8c83c0996675d0fdb7f5adf67d7aef84610481 1148 vdk_0.2.2-1.debian.tar.xz
Checksums-Sha256:
 793b794f29c7eb4bdef008297ad2e0dad4a63c5b55aa26a9472dd2efb74c3e5c 87995 vdk_0.2.2.orig.tar.gz
 410c12ad0de26398141b6dcf6d201768af171808c07897066b2e6aab48284825 1148 vdk_0.2.2-1.debian.tar.xz
Files:
 f90a63d1dce336588799096904945a9a 87995 vdk_0.2.2.orig.tar.gz
 c424b02d22d6eeb30b7b758ba9e7df95 1148 vdk_0.2.2-1.debian.tar.xz
