Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.24-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 d5a2872847b2e6c60f39b6e89406edb81b33d933 73625 vdk_0.0.24.orig.tar.gz
 7416d2b427d1e2a6cbc6fe138c18e2150c3a3443 1028 vdk_0.0.24-1.debian.tar.xz
Checksums-Sha256:
 d2914af502cbba0e0041aa4133075d2c85814383a0706319b52defb0485b0e0d 73625 vdk_0.0.24.orig.tar.gz
 b0e1e30ba1286cee1e7dc5289ef2e1ff34be241cd9c6309adcc8fa7669150267 1028 vdk_0.0.24-1.debian.tar.xz
Files:
 10ff7c0bd675e14d5058a8dff8290b58 73625 vdk_0.0.24.orig.tar.gz
 078e0386c7a1ba2598c494e951b72526 1028 vdk_0.0.24-1.debian.tar.xz
