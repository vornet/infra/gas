Format: 3.0 (quilt)
Source: vdk
Binary: vdk-common, vdk-pylib, vdk-zigbin
Architecture: all
Version: 0.2.7-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
 vdk-zigbin deb misc extra arch=all
Checksums-Sha1:
 54b5c1ab178537670f291d8c1420bea6ded73ca7 92190 vdk_0.2.7.orig.tar.gz
 3d6ee00e6745fa7777475b08f92836779aa42c02 1240 vdk_0.2.7-1.debian.tar.xz
Checksums-Sha256:
 58c37d900175fe8518a15a1e25c7bc12ae2a68d1595f7b857c99f4a3ad4b09cd 92190 vdk_0.2.7.orig.tar.gz
 300a619a5ca07dd1e482bde233a57bb1134a3ec72297d7c9c3e0dce327636a3a 1240 vdk_0.2.7-1.debian.tar.xz
Files:
 0d5f74434a59e06e081126bd0a51e4ff 92190 vdk_0.2.7.orig.tar.gz
 a99a5f2c2448384ae9ad0e5f7f74e9ea 1240 vdk_0.2.7-1.debian.tar.xz
