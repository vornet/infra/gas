Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.62-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 435481c6ad1344fbb213d97bd0593a9856d50b0d 93903 shellfu_0.10.62.orig.tar.gz
 f917a8b0867f77fa497a964ec11a85a0b3e2a3d1 2228 shellfu_0.10.62-1.debian.tar.xz
Checksums-Sha256:
 6bc868fc5bb976044c6117c0acb9b2bceab8c412e7dd6e0cdb0e1243a9d542fe 93903 shellfu_0.10.62.orig.tar.gz
 89d43f124af9b248fb1af053959127ad5e7666135715c37c00ea089bf1da15fa 2228 shellfu_0.10.62-1.debian.tar.xz
Files:
 cdfa63f2baf870592ab6a43a2b831a05 93903 shellfu_0.10.62.orig.tar.gz
 bae02e0e91e4a952b212ed0684414720 2228 shellfu_0.10.62-1.debian.tar.xz
