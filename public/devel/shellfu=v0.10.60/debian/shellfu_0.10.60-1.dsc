Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.60-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 2eae31047a4f7c632aa8a5e2fe6f2cd236a6b667 93127 shellfu_0.10.60.orig.tar.gz
 936f9c26b349360f2e0d59e189d8174adde6c73f 2224 shellfu_0.10.60-1.debian.tar.xz
Checksums-Sha256:
 fa1925e2390d57281f68ec3333abdf22dd63f3de5c011f341f7c7d69e81371c8 93127 shellfu_0.10.60.orig.tar.gz
 67cfe427422dc786ff51f587c85389ad834d5d9d96334120981cbb2cdc480d7d 2224 shellfu_0.10.60-1.debian.tar.xz
Files:
 255e804451c91e8334c3a5265cfa31c5 93127 shellfu_0.10.60.orig.tar.gz
 1cc3983f0ad26c825725f67b8537694a 2224 shellfu_0.10.60-1.debian.tar.xz
