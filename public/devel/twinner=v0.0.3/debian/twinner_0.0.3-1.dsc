Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.3-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 a428c91563da26cd32e4d77d892c89d9a6daed8d 33454 twinner_0.0.3.orig.tar.gz
 0d1915256d075fc22c0a763038fa9970de123446 800 twinner_0.0.3-1.debian.tar.xz
Checksums-Sha256:
 44894d62729c1152cc08fb8c11b33ebeec60b4a84500f691a7c2d77f3e9bbfb9 33454 twinner_0.0.3.orig.tar.gz
 23e5b2a7bc045bcce4d6961a022ee9014e2152516d2dbc34c7c0192dd133ee99 800 twinner_0.0.3-1.debian.tar.xz
Files:
 2f8622d0154957188350d573b6653125 33454 twinner_0.0.3.orig.tar.gz
 69fb3cde06ee536f81bffe7a57e55751 800 twinner_0.0.3-1.debian.tar.xz
