Format: 3.0 (quilt)
Source: shellfu-sh-mdfmt
Binary: shellfu-sh-mdfmt
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+mdfmt@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-sh-mdfmt
Build-Depends: debhelper (>= 9), shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-sh
Package-List:
 shellfu-sh-mdfmt deb misc extra arch=all
Checksums-Sha1:
 30662077b852b3344bd051d02f9eb7b909ae9401 50252 shellfu-sh-mdfmt_0.0.2.orig.tar.gz
 cab843a2fd3f1910a61b612aed3c524d705d5844 948 shellfu-sh-mdfmt_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 ea2614a2f8f0a8157e8e16377a55e5addaadf76a21855a12e4085f84eaa4fd68 50252 shellfu-sh-mdfmt_0.0.2.orig.tar.gz
 64c01142c2632e218000e00475bbc21326ace226a804c81a741dd82feea295ef 948 shellfu-sh-mdfmt_0.0.2-1.debian.tar.xz
Files:
 d1a323c9fc5fa4ddc9f2681bd63b407f 50252 shellfu-sh-mdfmt_0.0.2.orig.tar.gz
 dbe06583b13068927cc1ea0153d951be 948 shellfu-sh-mdfmt_0.0.2-1.debian.tar.xz
