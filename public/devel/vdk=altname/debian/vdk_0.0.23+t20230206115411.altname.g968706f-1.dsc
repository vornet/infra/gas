Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.23+t20230206115411.altname.g968706f-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 16f86ac7635860ba88c69d30546f841573973a3d 71296 vdk_0.0.23+t20230206115411.altname.g968706f.orig.tar.gz
 05d85a89625053400a0eb6d2efd434535c25f40e 1048 vdk_0.0.23+t20230206115411.altname.g968706f-1.debian.tar.xz
Checksums-Sha256:
 fe5858eccf1d32b93cae159852bb01967db25538d43654412340f423d8e47dc9 71296 vdk_0.0.23+t20230206115411.altname.g968706f.orig.tar.gz
 21ceb82bbfd170d3b68d894ac1c029a711382cf389d65dbadf0df847a9990419 1048 vdk_0.0.23+t20230206115411.altname.g968706f-1.debian.tar.xz
Files:
 0bcc37981bddba1a2008661e4fc7a7d7 71296 vdk_0.0.23+t20230206115411.altname.g968706f.orig.tar.gz
 007faa5d11acfd6d5f8a30acf50c96a5 1048 vdk_0.0.23+t20230206115411.altname.g968706f-1.debian.tar.xz
