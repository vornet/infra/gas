Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.51+t20220112110916.mvsd.gc1245e8-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 677005a191f71d10736e135ed2bf31b29705b072 93892 shellfu_0.10.51+t20220112110916.mvsd.gc1245e8.orig.tar.gz
 5acee0b32d46f384278567477ad6ce8ad7d51200 2244 shellfu_0.10.51+t20220112110916.mvsd.gc1245e8-1.debian.tar.xz
Checksums-Sha256:
 21bc29e708908368ddc8bb04dfb3a01b2594f8954e1d517ae71cdb7fc8ce378b 93892 shellfu_0.10.51+t20220112110916.mvsd.gc1245e8.orig.tar.gz
 5d0c3ee7df3ad9670c690058930e6af637db3afd84eeae1bd74f22b1a0554e62 2244 shellfu_0.10.51+t20220112110916.mvsd.gc1245e8-1.debian.tar.xz
Files:
 733629866e9555362bd5c25e9ffac526 93892 shellfu_0.10.51+t20220112110916.mvsd.gc1245e8.orig.tar.gz
 898d2e2e4e9e8660a87b762865abcea9 2244 shellfu_0.10.51+t20220112110916.mvsd.gc1245e8-1.debian.tar.xz
