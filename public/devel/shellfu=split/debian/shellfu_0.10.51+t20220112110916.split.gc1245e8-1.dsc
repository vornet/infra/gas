Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.51+t20220112110916.split.gc1245e8-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 65e5a97f10917f05d43ffdc05e17939e6b17f74c 94066 shellfu_0.10.51+t20220112110916.split.gc1245e8.orig.tar.gz
 af150e8de949a553d572f38e1c9895de7f8c41fc 2248 shellfu_0.10.51+t20220112110916.split.gc1245e8-1.debian.tar.xz
Checksums-Sha256:
 b12ee53d2916e990cc0c361268ade8682ba902df9df35e62ec6219a23903fcd4 94066 shellfu_0.10.51+t20220112110916.split.gc1245e8.orig.tar.gz
 2fd272e427039682512eb3b4bf6199f0c8c87b880b89bbffa8b0671ad546b811 2248 shellfu_0.10.51+t20220112110916.split.gc1245e8-1.debian.tar.xz
Files:
 2b679f3df840155f162b4dc290f64d86 94066 shellfu_0.10.51+t20220112110916.split.gc1245e8.orig.tar.gz
 91b94623208f05896a1bd361dd5ae4c7 2248 shellfu_0.10.51+t20220112110916.split.gc1245e8-1.debian.tar.xz
