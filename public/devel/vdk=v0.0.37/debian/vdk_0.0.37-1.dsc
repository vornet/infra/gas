Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.37-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 81bd4fb5172db42d1718e25a042c55b3d8129e45 75958 vdk_0.0.37.orig.tar.gz
 aa55614c8e2b43863f7d4ac216ae651cd41d1a7f 1092 vdk_0.0.37-1.debian.tar.xz
Checksums-Sha256:
 4e569ee2079f977c8d8dbf1d8a399812e660714c51caf06f79b0b5226069a70a 75958 vdk_0.0.37.orig.tar.gz
 3a8eac2651297932c7921faf3a56fe81f100588cb850eeb0e3380c6fbc55955a 1092 vdk_0.0.37-1.debian.tar.xz
Files:
 47967074271246874e0eb836451cc8e2 75958 vdk_0.0.37.orig.tar.gz
 b4ed1e7d7d61a046fda928caf5c54efc 1092 vdk_0.0.37-1.debian.tar.xz
