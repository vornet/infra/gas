Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.21-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 82a3d24bd5ad337405c67857f1078a8a7d44f6ae 72614 bmo_0.1.21.orig.tar.gz
 526a84ed9f6aef58b8478b5e401383fbf259d77a 1604 bmo_0.1.21-1.debian.tar.xz
Checksums-Sha256:
 a9b96a0cb4400a8f3e35d4253cf4fc46b6a699298c3c9a7ebdefea88e3a85451 72614 bmo_0.1.21.orig.tar.gz
 56a939426031eb719ec5d01e876099892ca60661a446a17b8aafbe07e9bb19b7 1604 bmo_0.1.21-1.debian.tar.xz
Files:
 0dfd0c6cc3b4b1a81b56a8cb17f82223 72614 bmo_0.1.21.orig.tar.gz
 f4bf0a232cf745cd19069425a84ca188 1604 bmo_0.1.21-1.debian.tar.xz
