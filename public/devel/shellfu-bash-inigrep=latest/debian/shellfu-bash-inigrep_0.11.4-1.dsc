Format: 3.0 (quilt)
Source: shellfu-bash-inigrep
Binary: shellfu-bash-inigrep
Architecture: all
Version: 0.11.4-1
Maintainer: Alois Mahdal <netvor+inigrep@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-inigrep
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-inigrep deb misc extra arch=all
Checksums-Sha1:
 592e63351c56e97274c2827d00f9af6446e5eb82 76397 shellfu-bash-inigrep_0.11.4.orig.tar.gz
 9335973d8078ce81ba88907a5427b183c1fcbe9d 924 shellfu-bash-inigrep_0.11.4-1.debian.tar.xz
Checksums-Sha256:
 3dd2078b1cabe35c000013181def9fe64a7b6233aafb1424591456ed8d1759de 76397 shellfu-bash-inigrep_0.11.4.orig.tar.gz
 052356e5e2450f568b9ae136d6d27f9ef9c4cb4613e8a3f0140a09e0a5bb0de8 924 shellfu-bash-inigrep_0.11.4-1.debian.tar.xz
Files:
 44279141f82e72a737741f744b1e0a8d 76397 shellfu-bash-inigrep_0.11.4.orig.tar.gz
 0cf47f8a1618a446fe20f8c43e2c766f 924 shellfu-bash-inigrep_0.11.4-1.debian.tar.xz
