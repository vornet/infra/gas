Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.12-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 852c6a0cf814d5c9f2b6d20680d4fa5a77f56a38 101671 shellfu-bash-jat_0.2.12.orig.tar.gz
 84df44166cc0e0e0cb029a105f435d0296391cb2 1004 shellfu-bash-jat_0.2.12-1.debian.tar.xz
Checksums-Sha256:
 a4acd496bd5e0066a7334bc906d0c8ff7039cb341f522ad7caed936c1102abc7 101671 shellfu-bash-jat_0.2.12.orig.tar.gz
 f19021dfcd1087be86982a446445dff73533503def4d9854ade7ebb73c522d70 1004 shellfu-bash-jat_0.2.12-1.debian.tar.xz
Files:
 2e9ccf57fe224f3b2586cc6a45cb624e 101671 shellfu-bash-jat_0.2.12.orig.tar.gz
 63ee685edb553e0e8487faf2f4b5cece 1004 shellfu-bash-jat_0.2.12-1.debian.tar.xz
