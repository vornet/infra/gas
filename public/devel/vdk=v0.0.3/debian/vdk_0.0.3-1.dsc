Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.3-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper (>= 9), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 8afb48bb5e730a32cb3cabfdf0732a379edbd770 60940 vdk_0.0.3.orig.tar.gz
 82ab52570f5930df9cf639e71758989377439c08 992 vdk_0.0.3-1.debian.tar.xz
Checksums-Sha256:
 a0bbb034b084a0b7247c20f415b4759c74d1ae42fdc3994b993a2f51f5bdf2a8 60940 vdk_0.0.3.orig.tar.gz
 cb1a037d41e8c75281c2a90754d704642e6c6e305e8db165a32081119cdbf481 992 vdk_0.0.3-1.debian.tar.xz
Files:
 0e9c1d9bb2f0cb4b9937f5e7b69c8444 60940 vdk_0.0.3.orig.tar.gz
 60844fcbea8522eb940d4b0560aaa344 992 vdk_0.0.3-1.debian.tar.xz
