Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.3.0-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 11abb91cf2f3aea1a327af388d3c7a497fb99622 82782 bmo_0.3.0.orig.tar.gz
 65ea6222451124ada7481cdd6190cc5437a34dae 1696 bmo_0.3.0-1.debian.tar.xz
Checksums-Sha256:
 0ce245d8ab7fb65ac4781041b8f3ab866c5df2d444eb46a19f2aff38c80957de 82782 bmo_0.3.0.orig.tar.gz
 872ba99c049a2483590b6d2f41696f543ab60b7939d2ecdd0771c302f0040af4 1696 bmo_0.3.0-1.debian.tar.xz
Files:
 9450e5f3f4ce2ef88d11070f2828cb1d 82782 bmo_0.3.0.orig.tar.gz
 7572e5e3891aacd17ab74e6db6042f9a 1696 bmo_0.3.0-1.debian.tar.xz
