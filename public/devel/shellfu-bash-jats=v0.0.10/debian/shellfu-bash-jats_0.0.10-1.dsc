Format: 3.0 (quilt)
Source: shellfu-bash-jats
Binary: shellfu-bash-jats
Architecture: all
Version: 0.0.10-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jats
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.44), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty
Package-List:
 shellfu-bash-jats deb misc extra arch=all
Checksums-Sha1:
 e34f8b7112b67dfa02b4be5a8d50733dda4fe6e1 49460 shellfu-bash-jats_0.0.10.orig.tar.gz
 b63b7d2cddcbc8123aba7beec1805414729eb5f3 908 shellfu-bash-jats_0.0.10-1.debian.tar.xz
Checksums-Sha256:
 aaa2104b20eb2bf5b926cc69ef937fe25db4bf0fda7e8ea1795fad823b66a3bd 49460 shellfu-bash-jats_0.0.10.orig.tar.gz
 529fab211fcbfeb8d78c2b255b1e024044db4a2d9cf4fb7258f67c83d571264e 908 shellfu-bash-jats_0.0.10-1.debian.tar.xz
Files:
 e261d01bc0b5edc5b7741c458e0dc1cb 49460 shellfu-bash-jats_0.0.10.orig.tar.gz
 ebcc10f32604a6a9282400d1e552cb8b 908 shellfu-bash-jats_0.0.10-1.debian.tar.xz
