Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.6-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 47de3bfc4e352b0b9762639f99dca582693b93b2 60971 vdk_0.0.6.orig.tar.gz
 5effcb6afa9ecee5fc57c2539ca730a4347bc20f 948 vdk_0.0.6-1.debian.tar.xz
Checksums-Sha256:
 9c1c1c1e80689c5db5be0679bdb32752e239a90ed45d5a2eb4a7ad17f8beb385 60971 vdk_0.0.6.orig.tar.gz
 349273403876a3a3ddc96cc0076424b9a3142788e8d9ceb64047e85b79d35f28 948 vdk_0.0.6-1.debian.tar.xz
Files:
 970fa0fe37487356565fd80409f1fe90 60971 vdk_0.0.6.orig.tar.gz
 c67310211522197741a2f5ffb907d660 948 vdk_0.0.6-1.debian.tar.xz
