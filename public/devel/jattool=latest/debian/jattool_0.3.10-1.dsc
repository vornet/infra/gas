Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.10-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 92459e0e713025516da56b418f375b0aecc1e42f 71152 jattool_0.3.10.orig.tar.gz
 dd3d4f4e836c21cfd3cb44e23c9296b4d779b884 1488 jattool_0.3.10-1.debian.tar.xz
Checksums-Sha256:
 3a0280463dec509356d244298d9f31ec7ae882c3f2788e1ebb892d24a62457f9 71152 jattool_0.3.10.orig.tar.gz
 48cfa5b7cf519490d478dcec0f056d47af6f4453e2772186ef91f6b5fb4b1646 1488 jattool_0.3.10-1.debian.tar.xz
Files:
 72563baa812841b1800493c2918c2166 71152 jattool_0.3.10.orig.tar.gz
 76ed47ba595f7840db83adb977ec8e44 1488 jattool_0.3.10-1.debian.tar.xz
