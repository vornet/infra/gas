Format: 3.0 (quilt)
Source: shellfu-bash-cached
Binary: shellfu-bash-cached
Architecture: all
Version: 0.1.2-1
Maintainer: Alois Mahdal <netvor+cached@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-cached
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-cached deb misc extra arch=all
Checksums-Sha1:
 ac085bca3e4c9483522723ea21e321029ae691f7 36227 shellfu-bash-cached_0.1.2.orig.tar.gz
 1f5a1486f5ac7e2f23009dc548c862f2209971b7 876 shellfu-bash-cached_0.1.2-1.debian.tar.xz
Checksums-Sha256:
 601fa6352424200eb2ccecb1cd432cbfa495ca26f533ff70e90bd715b79ff9c1 36227 shellfu-bash-cached_0.1.2.orig.tar.gz
 85477a76b0610f3b0b341c5565f170fa6cff2d368868a468660f099e94da12ca 876 shellfu-bash-cached_0.1.2-1.debian.tar.xz
Files:
 aaa2ce78cc1b16df44053528f7a21c55 36227 shellfu-bash-cached_0.1.2.orig.tar.gz
 f810bb8cde8273a5f9a0d4029d6dcadc 876 shellfu-bash-cached_0.1.2-1.debian.tar.xz
