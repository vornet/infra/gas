Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.16-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 411a904a660afcced6841c145e24f0dc851424e3 66095 vdk_0.0.16.orig.tar.gz
 01647b219278b7bdf432116018cc461c1ac1d105 1012 vdk_0.0.16-1.debian.tar.xz
Checksums-Sha256:
 89f24eaf03e86c592b15068619b383312a3a22fa5d17aacdc08e19888d630cba 66095 vdk_0.0.16.orig.tar.gz
 0cdba0c9d1f15b64a808bb367697f28282304746e35454e4eca5a454d735e6c3 1012 vdk_0.0.16-1.debian.tar.xz
Files:
 2597b42942a47e2c6b0af23054b79b18 66095 vdk_0.0.16.orig.tar.gz
 2ea1ebc99da342ee5a6820cdc7d26d81 1012 vdk_0.0.16-1.debian.tar.xz
