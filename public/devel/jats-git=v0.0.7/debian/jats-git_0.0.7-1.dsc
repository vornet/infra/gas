Format: 3.0 (quilt)
Source: jats-git
Binary: jats-git
Architecture: all
Version: 0.0.7-1
Maintainer: Alois Mahdal <amahdal@redhat.com>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-git deb misc extra arch=all
Checksums-Sha1:
 e319871408c7b2a67e0071bdd6846ab7b91fcfb4 2118 jats-git_0.0.7.orig.tar.gz
 0017319557ebb9bf440284712e82c8d77ee83417 784 jats-git_0.0.7-1.debian.tar.xz
Checksums-Sha256:
 4a7373b921d5b29b76a450527183618893edc71b6eb3a61aba35f7cb4a3b72b9 2118 jats-git_0.0.7.orig.tar.gz
 21cd88f7c8d2087d79e25a16c955d93fca56d18963c33f345e1a7ab0324dd7b7 784 jats-git_0.0.7-1.debian.tar.xz
Files:
 e5ea9ac60ce2c912366da9d5fb752ae5 2118 jats-git_0.0.7.orig.tar.gz
 b4da73de7e262a1120a67f0ae46217e1 784 jats-git_0.0.7-1.debian.tar.xz
