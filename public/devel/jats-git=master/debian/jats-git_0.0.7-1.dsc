Format: 3.0 (quilt)
Source: jats-git
Binary: jats-git
Architecture: all
Version: 0.0.7-1
Maintainer: Alois Mahdal <amahdal@redhat.com>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-git deb misc extra arch=all
Checksums-Sha1:
 ea0cb762a3324447de8e0632a39c33fce7eea5ae 2114 jats-git_0.0.7.orig.tar.gz
 0017319557ebb9bf440284712e82c8d77ee83417 784 jats-git_0.0.7-1.debian.tar.xz
Checksums-Sha256:
 22bc72052dedccb29edf05b5b6c7ef89fd6ab4b8ec2f5310e2f84d02891c5bbc 2114 jats-git_0.0.7.orig.tar.gz
 21cd88f7c8d2087d79e25a16c955d93fca56d18963c33f345e1a7ab0324dd7b7 784 jats-git_0.0.7-1.debian.tar.xz
Files:
 a90b1eae324fda9faec9805b09abceea 2114 jats-git_0.0.7.orig.tar.gz
 b4da73de7e262a1120a67f0ae46217e1 784 jats-git_0.0.7-1.debian.tar.xz
