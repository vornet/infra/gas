Format: 3.0 (quilt)
Source: imapdomo
Binary: imapdomo
Architecture: all
Version: 0.0.20+t20240329131103.main.g3d0eb8e-1
Maintainer: Alois Mahdal <netvor+imapdomo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/hacktop/imapdomo
Build-Depends: debhelper (>= 9), make
Package-List:
 imapdomo deb misc extra arch=all
Checksums-Sha1:
 f89650f9d338501ee1b157a74cc3b9cc7496deba 31844 imapdomo_0.0.20+t20240329131103.main.g3d0eb8e.orig.tar.gz
 164bfb46f93f3650a0075efbdb6cd4aca78b4ca7 960 imapdomo_0.0.20+t20240329131103.main.g3d0eb8e-1.debian.tar.xz
Checksums-Sha256:
 4aad623b877f3e0c17b5a97e135bef2f04fa80117b651407c8a304c1a750d127 31844 imapdomo_0.0.20+t20240329131103.main.g3d0eb8e.orig.tar.gz
 c4727a96596a159420c5db484ff87b6414a0bfbb4e3c44827aed95e41ddabd8f 960 imapdomo_0.0.20+t20240329131103.main.g3d0eb8e-1.debian.tar.xz
Files:
 3a2f135cbb3be440744fc5541e57763f 31844 imapdomo_0.0.20+t20240329131103.main.g3d0eb8e.orig.tar.gz
 a1a4f6310166e3872a1cd47e769edebe 960 imapdomo_0.0.20+t20240329131103.main.g3d0eb8e-1.debian.tar.xz
