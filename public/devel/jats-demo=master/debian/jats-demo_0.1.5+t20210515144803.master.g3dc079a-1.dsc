Format: 3.0 (quilt)
Source: jats-demo
Binary: jats-demo
Architecture: all
Version: 0.1.5+t20210515144803.master.g3dc079a-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-demo deb misc extra arch=all
Checksums-Sha1:
 d1bacb77e85c3476168ae26895c246a2f2dd6376 6147 jats-demo_0.1.5+t20210515144803.master.g3dc079a.orig.tar.gz
 cdbec591376f28d0a6d4ac16cf39fa040156429a 812 jats-demo_0.1.5+t20210515144803.master.g3dc079a-1.debian.tar.xz
Checksums-Sha256:
 674fe330f094b4065399d2cf7c5e76b9c70ddfe65db91b789115a90232a08606 6147 jats-demo_0.1.5+t20210515144803.master.g3dc079a.orig.tar.gz
 feea839782e65a7b5929366bfdecc31332ebc7d9a460fe60e925f972da85461b 812 jats-demo_0.1.5+t20210515144803.master.g3dc079a-1.debian.tar.xz
Files:
 41f7285f813957ca483b4a7184abd7d1 6147 jats-demo_0.1.5+t20210515144803.master.g3dc079a.orig.tar.gz
 a9e36ed9fc3d47e93a2838bedc23df67 812 jats-demo_0.1.5+t20210515144803.master.g3dc079a-1.debian.tar.xz
