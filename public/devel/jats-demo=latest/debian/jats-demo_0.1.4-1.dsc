Format: 3.0 (quilt)
Source: jats-demo
Binary: jats-demo
Architecture: all
Version: 0.1.4-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-demo deb misc extra arch=all
Checksums-Sha1:
 7e0a3f7089ceb5719c17fa687d4d5c8262e628db 6115 jats-demo_0.1.4.orig.tar.gz
 872d57f80d92e19b03a5adf4d487abff3773c45b 812 jats-demo_0.1.4-1.debian.tar.xz
Checksums-Sha256:
 65e7da58c92e94901b615f297f7ea3999f602a89e94399c23537c89aac6472a4 6115 jats-demo_0.1.4.orig.tar.gz
 2fbd17b19ee74e92ea7fae133271b01ebf4cb61a46d28c6d5f86595dcfd77113 812 jats-demo_0.1.4-1.debian.tar.xz
Files:
 59cc8a4760ce241448b6c6dcfcc65194 6115 jats-demo_0.1.4.orig.tar.gz
 04b172f45f31f0028fd15861bcaed7ec 812 jats-demo_0.1.4-1.debian.tar.xz
