Format: 3.0 (quilt)
Source: vdk
Binary: vdk-common, vdk-pylib, vdk-zigbin
Architecture: all
Version: 0.2.5-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
 vdk-zigbin deb misc extra arch=all
Checksums-Sha1:
 aad13ec89d27fbb15f3cded62ef67cdc9ccc90e5 90487 vdk_0.2.5.orig.tar.gz
 c0ab1c65596381861412585ef0d3d069fed00f44 1228 vdk_0.2.5-1.debian.tar.xz
Checksums-Sha256:
 d02ae154df465aca473c055abc60fc0f091dd7e4b388932b59e30b8ad65d4f1c 90487 vdk_0.2.5.orig.tar.gz
 cb096d6f97ea0ff460c9c7f036c45eb6a2714ff30b5dd8aea35e299ec4ec5232 1228 vdk_0.2.5-1.debian.tar.xz
Files:
 415c4b886a60f669c29beef49433b1a8 90487 vdk_0.2.5.orig.tar.gz
 2024b25cd583c6ad52cd60c286ce8baf 1228 vdk_0.2.5-1.debian.tar.xz
