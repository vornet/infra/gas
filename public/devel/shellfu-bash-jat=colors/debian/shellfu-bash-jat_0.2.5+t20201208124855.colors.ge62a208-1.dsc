Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.5+t20201208124855.colors.ge62a208-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 fc733ee804ba886e0ff92533795b5aef0082b150 81814 shellfu-bash-jat_0.2.5+t20201208124855.colors.ge62a208.orig.tar.gz
 cb7f996160cc804a34d8a213c8b730c51030b6d9 1016 shellfu-bash-jat_0.2.5+t20201208124855.colors.ge62a208-1.debian.tar.xz
Checksums-Sha256:
 c2bf35268aab027a1c8a0220bcf6f6f8c1adc99c9ba0bc899f022ded06e9088c 81814 shellfu-bash-jat_0.2.5+t20201208124855.colors.ge62a208.orig.tar.gz
 7463bb2cdec27b38b90278a9d60e7516e3b11bb649834910f44db1fae7394197 1016 shellfu-bash-jat_0.2.5+t20201208124855.colors.ge62a208-1.debian.tar.xz
Files:
 86078e9a41dd7413f0936132dc931af9 81814 shellfu-bash-jat_0.2.5+t20201208124855.colors.ge62a208.orig.tar.gz
 2b3ca16f0adc7ef6637dc792e502b700 1016 shellfu-bash-jat_0.2.5+t20201208124855.colors.ge62a208-1.debian.tar.xz
