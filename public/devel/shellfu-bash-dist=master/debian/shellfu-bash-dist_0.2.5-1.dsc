Format: 3.0 (quilt)
Source: shellfu-bash-dist
Binary: shellfu-bash-dist
Architecture: all
Version: 0.2.5-1
Maintainer: Alois Mahdal <netvor+dist@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-dist
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-dist deb misc extra arch=all
Checksums-Sha1:
 28a4f85fce6be316b86742ed201a03a76ed4299c 47601 shellfu-bash-dist_0.2.5.orig.tar.gz
 edf1b2121d5584f61bdbc7f1f3f776e65beffc3c 860 shellfu-bash-dist_0.2.5-1.debian.tar.xz
Checksums-Sha256:
 2b9f74a3e9ff409cbc969226875d38a092624455bdf7ae5ff553d99eece6708f 47601 shellfu-bash-dist_0.2.5.orig.tar.gz
 ce44e278b6e38aee670bc0814513e9fdb221b5d5bb0c1e7f11da89edb09c0ccd 860 shellfu-bash-dist_0.2.5-1.debian.tar.xz
Files:
 07a0773bc199dea2fc309b6a8b3cc629 47601 shellfu-bash-dist_0.2.5.orig.tar.gz
 640b6289372af1be12d1d9b95b648d4d 860 shellfu-bash-dist_0.2.5-1.debian.tar.xz
