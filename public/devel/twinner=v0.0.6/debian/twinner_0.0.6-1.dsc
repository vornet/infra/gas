Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.6-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 7001920c4183303f33293aabeab7cd4e14f6a379 33859 twinner_0.0.6.orig.tar.gz
 11cd30a64c5d76ec7ba529aabc691df7a911e918 804 twinner_0.0.6-1.debian.tar.xz
Checksums-Sha256:
 fde1f3d4a5567f8c95f8c4dd52e350af1bcbe2ba138ffbdb4f340ed5ddbc56ff 33859 twinner_0.0.6.orig.tar.gz
 845862f78383df3f87fef36d58c5b647361a3ebae5ca1fc95f6dda4881e3202f 804 twinner_0.0.6-1.debian.tar.xz
Files:
 b382b43e3e759e2897a7168d27646e5e 33859 twinner_0.0.6.orig.tar.gz
 9f9a329eda0d8febc5fa3ada02cabbc4 804 twinner_0.0.6-1.debian.tar.xz
