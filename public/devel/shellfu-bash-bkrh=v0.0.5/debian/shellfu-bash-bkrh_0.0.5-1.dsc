Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 a5e439304dbd5aa1811e384e2f76de3898ed3145 38210 shellfu-bash-bkrh_0.0.5.orig.tar.gz
 83479bddfc68f513ee729bb749d0dba94c46c853 820 shellfu-bash-bkrh_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 5c405ac8d3d400b0ffbbb1987c3a7cfb4775fe92eb9d6285b38f38e5e2917905 38210 shellfu-bash-bkrh_0.0.5.orig.tar.gz
 6ee6c74f477ee4695f0ef0edbcada5d437cea2f5e02a0ebd595c2c2aa6da5438 820 shellfu-bash-bkrh_0.0.5-1.debian.tar.xz
Files:
 2aaaec3a7e2bfde7e578cf890fb4e045 38210 shellfu-bash-bkrh_0.0.5.orig.tar.gz
 b905d776f7593df924e95dfc9a6eb47a 820 shellfu-bash-bkrh_0.0.5-1.debian.tar.xz
