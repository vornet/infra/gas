Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.7-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 8a3832cfe56956100cfdfa98dfa770d9f41a02a4 83081 shellfu-bash-jat_0.2.7.orig.tar.gz
 eee75922f7df8927c3f0ed5da2c25be3a935b4f2 1004 shellfu-bash-jat_0.2.7-1.debian.tar.xz
Checksums-Sha256:
 98f191b1cb0970d795b807b517efca290095ef9b0ddc93db769f4746e95529ed 83081 shellfu-bash-jat_0.2.7.orig.tar.gz
 0469a8bb84df1cb1d8c835d0a9236892c192fd8dc4f33b257c73df045b12cd3d 1004 shellfu-bash-jat_0.2.7-1.debian.tar.xz
Files:
 d8467ce6414e10a2a4001db646807100 83081 shellfu-bash-jat_0.2.7.orig.tar.gz
 feb4507622d68798b006a33f3bfcc972 1004 shellfu-bash-jat_0.2.7-1.debian.tar.xz
