Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.9-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 87bf102f1dbb230bc03cf2e1648a0edc01e9aac1 61052 vdk_0.0.9.orig.tar.gz
 352fef80af03038d61ee8308d726977e3c09eaef 948 vdk_0.0.9-1.debian.tar.xz
Checksums-Sha256:
 07a7afaf09faa7c84db553a1b9cabd19142869707e844d229338b35894051981 61052 vdk_0.0.9.orig.tar.gz
 14b571abf750e05e85e95fb5e7b7572a65ac7c45de8155716d042e5d5aa63767 948 vdk_0.0.9-1.debian.tar.xz
Files:
 a63c56c33989ca1b0f6eb68c2113b2d2 61052 vdk_0.0.9.orig.tar.gz
 a79fa96a6516ffa50346eee25bd1ce38 948 vdk_0.0.9-1.debian.tar.xz
