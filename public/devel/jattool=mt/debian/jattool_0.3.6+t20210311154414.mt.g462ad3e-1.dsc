Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.6+t20210311154414.mt.g462ad3e-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 bb47866c39491177523e38c586fca8db96564936 69501 jattool_0.3.6+t20210311154414.mt.g462ad3e.orig.tar.gz
 38e25821bb09a568dd0d9cc68f1c3ed7f438501e 1508 jattool_0.3.6+t20210311154414.mt.g462ad3e-1.debian.tar.xz
Checksums-Sha256:
 d3789c5a5b5017eb76a1ca11faf94e3756d0fc0a543b0a4b2bbe5acb5a4da82e 69501 jattool_0.3.6+t20210311154414.mt.g462ad3e.orig.tar.gz
 a23f885910e1bb5b54d711be84bf972f0e3a35d00d27c05f9db1fc97e818ade0 1508 jattool_0.3.6+t20210311154414.mt.g462ad3e-1.debian.tar.xz
Files:
 ba89b48a415138ce64174e67153fef70 69501 jattool_0.3.6+t20210311154414.mt.g462ad3e.orig.tar.gz
 7ee69ac95bf33a915a64d46c67316447 1508 jattool_0.3.6+t20210311154414.mt.g462ad3e-1.debian.tar.xz
