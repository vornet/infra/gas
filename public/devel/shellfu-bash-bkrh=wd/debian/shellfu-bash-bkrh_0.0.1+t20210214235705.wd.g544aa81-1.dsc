Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.1+t20210214235705.wd.g544aa81-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 5f464419dfc761e17cb906050cdc3eb3ba39fc9f 34183 shellfu-bash-bkrh_0.0.1+t20210214235705.wd.g544aa81.orig.tar.gz
 663aa9c630e76fafae9e6254204f51a2f43c22eb 836 shellfu-bash-bkrh_0.0.1+t20210214235705.wd.g544aa81-1.debian.tar.xz
Checksums-Sha256:
 56208872e5b7220cabe24c8f38529654fa2dcfa6b7e73ed43df595de8211bbbf 34183 shellfu-bash-bkrh_0.0.1+t20210214235705.wd.g544aa81.orig.tar.gz
 0be50d626d9046cb9bfe8c5228e1468d5a32295b4574635d870c271b6508f037 836 shellfu-bash-bkrh_0.0.1+t20210214235705.wd.g544aa81-1.debian.tar.xz
Files:
 07ab8d94188cca9ef20526cbf8437016 34183 shellfu-bash-bkrh_0.0.1+t20210214235705.wd.g544aa81.orig.tar.gz
 3cd88ec401f06b84f8bc008dcab7deaa 836 shellfu-bash-bkrh_0.0.1+t20210214235705.wd.g544aa81-1.debian.tar.xz
