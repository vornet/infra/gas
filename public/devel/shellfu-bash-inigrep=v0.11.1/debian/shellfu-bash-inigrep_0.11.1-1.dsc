Format: 3.0 (quilt)
Source: shellfu-bash-inigrep
Binary: shellfu-bash-inigrep
Architecture: all
Version: 0.11.1-1
Maintainer: Alois Mahdal <netvor+inigrep@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-inigrep
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-inigrep deb misc extra arch=all
Checksums-Sha1:
 0494180d74bef06359ce80430e2622af93eab35e 71796 shellfu-bash-inigrep_0.11.1.orig.tar.gz
 6ea71d3fad005c2a1c170e113bc4b2b21dbedb56 924 shellfu-bash-inigrep_0.11.1-1.debian.tar.xz
Checksums-Sha256:
 0f03611d51188eab6b39f9fad4ef15288b5bb612e2332dcab7f8bb2156317f3e 71796 shellfu-bash-inigrep_0.11.1.orig.tar.gz
 c438c04d754effd9b100794d590875c6f87a17559e0d062e5e9cae5f27b2a03e 924 shellfu-bash-inigrep_0.11.1-1.debian.tar.xz
Files:
 e86ffca1e65114e203f481bf469e18a4 71796 shellfu-bash-inigrep_0.11.1.orig.tar.gz
 7489d3f75846752916b927965eb4e46a 924 shellfu-bash-inigrep_0.11.1-1.debian.tar.xz
