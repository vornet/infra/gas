Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.16+t20211002123602.scfx.gbe5e685-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), shellfu (>= 0.10.11), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0), xclip
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 1b5bffebe682fc1066bf44f87fb2aa871dfdc0a6 69010 bmo_0.1.16+t20211002123602.scfx.gbe5e685.orig.tar.gz
 f7a540b2e9eb4f9b5ca0c5bb94081903adab9c76 1624 bmo_0.1.16+t20211002123602.scfx.gbe5e685-1.debian.tar.xz
Checksums-Sha256:
 8a04dcb56f212e2271861c18e41475f7994ee72d8b64f927d14bb2a9311e68a4 69010 bmo_0.1.16+t20211002123602.scfx.gbe5e685.orig.tar.gz
 931c64caca6323b56b4933f111cc71caac501010826fe1d1ccffaa125fdc092b 1624 bmo_0.1.16+t20211002123602.scfx.gbe5e685-1.debian.tar.xz
Files:
 2265b496e4ecb676d9d971a98edf01b4 69010 bmo_0.1.16+t20211002123602.scfx.gbe5e685.orig.tar.gz
 68be850b6cc83ff5bdb545a54e5da3de 1624 bmo_0.1.16+t20211002123602.scfx.gbe5e685-1.debian.tar.xz
