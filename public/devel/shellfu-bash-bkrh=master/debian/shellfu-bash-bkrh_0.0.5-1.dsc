Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 2b208a2d4d105b119424983716d342c34534daf8 38212 shellfu-bash-bkrh_0.0.5.orig.tar.gz
 83479bddfc68f513ee729bb749d0dba94c46c853 820 shellfu-bash-bkrh_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 bcfc20128fbae223eb697b103d7c096583085ba5ef650b80173fa03f7dca9448 38212 shellfu-bash-bkrh_0.0.5.orig.tar.gz
 6ee6c74f477ee4695f0ef0edbcada5d437cea2f5e02a0ebd595c2c2aa6da5438 820 shellfu-bash-bkrh_0.0.5-1.debian.tar.xz
Files:
 17933570c0c911b2ee61194e30939ff9 38212 shellfu-bash-bkrh_0.0.5.orig.tar.gz
 b905d776f7593df924e95dfc9a6eb47a 820 shellfu-bash-bkrh_0.0.5-1.debian.tar.xz
