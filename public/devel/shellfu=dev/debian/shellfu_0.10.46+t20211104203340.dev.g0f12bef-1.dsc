Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.46+t20211104203340.dev.g0f12bef-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 f7ca43babaf8ec41494e314f834799de85c05b15 85864 shellfu_0.10.46+t20211104203340.dev.g0f12bef.orig.tar.gz
 2ba1be14a52db8312b3ac0957ed7da7fe7800c13 2228 shellfu_0.10.46+t20211104203340.dev.g0f12bef-1.debian.tar.xz
Checksums-Sha256:
 c54098ff09e959fde8fe237c18524e2f97c1cee51fa5d3172cb32c2af4f1e853 85864 shellfu_0.10.46+t20211104203340.dev.g0f12bef.orig.tar.gz
 ada99b5a8d645b6fe5aa229fb7f3d8f44eed5d8959d3342a6f326df667542340 2228 shellfu_0.10.46+t20211104203340.dev.g0f12bef-1.debian.tar.xz
Files:
 fe5dbe04fb5ab61eedc5805a44931182 85864 shellfu_0.10.46+t20211104203340.dev.g0f12bef.orig.tar.gz
 49f4e8432ed3f8da0908b34b01b15925 2228 shellfu_0.10.46+t20211104203340.dev.g0f12bef-1.debian.tar.xz
