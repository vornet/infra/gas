Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.2.2-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 4485919363b8e312adb12bd30e796bfbd695e603 88655 bmo_0.2.2.orig.tar.gz
 05634b5e0dc0a9313fff695bea06834d1d071cd4 1748 bmo_0.2.2-1.debian.tar.xz
Checksums-Sha256:
 1f4c82d544f637533afeed1e36ded71a3e702b927f851e55e740c76244c0ffb8 88655 bmo_0.2.2.orig.tar.gz
 89ba8b6380aa2b059b821e1ba5b528ff1fac284810ba73d97f020b53a8e10148 1748 bmo_0.2.2-1.debian.tar.xz
Files:
 0996e4976a1b600da1cb65b1fc9305ef 88655 bmo_0.2.2.orig.tar.gz
 99a2cb9d2fb416213521b2653399a000 1748 bmo_0.2.2-1.debian.tar.xz
