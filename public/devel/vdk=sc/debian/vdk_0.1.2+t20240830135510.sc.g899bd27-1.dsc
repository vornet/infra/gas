Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-common, vdk-pylib
Architecture: all
Version: 0.1.2+t20240830135510.sc.g899bd27-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 79fa2d1bcc27c62138f666aceaa0027165fc2856 88140 vdk_0.1.2+t20240830135510.sc.g899bd27.orig.tar.gz
 087df27b21731c433a7a5dbf81380d1594e78d92 1168 vdk_0.1.2+t20240830135510.sc.g899bd27-1.debian.tar.xz
Checksums-Sha256:
 d9ccfc41f67e70f4f7816b38a711ed5c4f4fefabc8ba85300391d33767122e6c 88140 vdk_0.1.2+t20240830135510.sc.g899bd27.orig.tar.gz
 a0a340dbb873f283830d759c790da5b943759fc17ab6234928168cf35d14d764 1168 vdk_0.1.2+t20240830135510.sc.g899bd27-1.debian.tar.xz
Files:
 c51df6cb34c85ec1bf26d8f6b5c191d6 88140 vdk_0.1.2+t20240830135510.sc.g899bd27.orig.tar.gz
 453428e51c0410a2eb30324520fd14a3 1168 vdk_0.1.2+t20240830135510.sc.g899bd27-1.debian.tar.xz
