Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-o, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.46+t20211005120625.o.ga02c8f9-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-o deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 a6cd1cf54d2ba0178f056b289a0f599f4cbd4622 86250 shellfu_0.10.46+t20211005120625.o.ga02c8f9.orig.tar.gz
 1a80a9c3076d228671721f808d2f1ba4f0f8782c 2292 shellfu_0.10.46+t20211005120625.o.ga02c8f9-1.debian.tar.xz
Checksums-Sha256:
 beafa63d69c414de7d34994ffb4d726444b1638ff81b866c3363b23c6f4a1ee6 86250 shellfu_0.10.46+t20211005120625.o.ga02c8f9.orig.tar.gz
 e404a74f7e2de4d08f1acd79f54c1d9e2413ffb6f1f3b1ef22f7c16bdb5ddc32 2292 shellfu_0.10.46+t20211005120625.o.ga02c8f9-1.debian.tar.xz
Files:
 3b17af6291e54ab721229b9606e12ad5 86250 shellfu_0.10.46+t20211005120625.o.ga02c8f9.orig.tar.gz
 b80285150ef8e19859df447e3e0f93a6 2292 shellfu_0.10.46+t20211005120625.o.ga02c8f9-1.debian.tar.xz
