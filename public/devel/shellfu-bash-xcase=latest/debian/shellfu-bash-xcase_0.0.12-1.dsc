Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.12-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 28abb7101f6df59fc403c94886dc66cc1760e42d 44918 shellfu-bash-xcase_0.0.12.orig.tar.gz
 218f98668f1a6876f5a450c840072ac04c2be0eb 1016 shellfu-bash-xcase_0.0.12-1.debian.tar.xz
Checksums-Sha256:
 004eb0848bc64804b5c2323221486f73f66f69593078b6055a2fd760a2b06a2b 44918 shellfu-bash-xcase_0.0.12.orig.tar.gz
 a73257be041fe1974293fe5bd518e0845e48a44295d2507d8ba31f1803a62f54 1016 shellfu-bash-xcase_0.0.12-1.debian.tar.xz
Files:
 8f3bf3d6b62006e86d0f637249a34541 44918 shellfu-bash-xcase_0.0.12.orig.tar.gz
 cd8785260e5c96aa7be7919ad08fb125 1016 shellfu-bash-xcase_0.0.12-1.debian.tar.xz
