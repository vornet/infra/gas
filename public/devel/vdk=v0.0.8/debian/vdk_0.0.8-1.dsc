Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.8-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 7f80087921e7ed230fa3e377dea2d5843473d65e 61055 vdk_0.0.8.orig.tar.gz
 5824c9dcf261633f2ed8d5988cc513db29d6b6bc 948 vdk_0.0.8-1.debian.tar.xz
Checksums-Sha256:
 6a8994c6e23e6544fdeae75bba8bae88462cb75fb6e0958a0bea5ff6280e3f08 61055 vdk_0.0.8.orig.tar.gz
 6043ca0a462e05d6d7b3a95509b18a31593c5a7974c2344a3e926133de1845c9 948 vdk_0.0.8-1.debian.tar.xz
Files:
 d495150c22500ca035757a9fa914362b 61055 vdk_0.0.8.orig.tar.gz
 b3562961675625c37cb0931df0e962d1 948 vdk_0.0.8-1.debian.tar.xz
