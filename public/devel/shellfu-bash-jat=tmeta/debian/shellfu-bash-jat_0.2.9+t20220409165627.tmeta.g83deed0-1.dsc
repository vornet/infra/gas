Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.9+t20220409165627.tmeta.g83deed0-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 d325299ab53f015f671d736027fef0930514cdd0 97847 shellfu-bash-jat_0.2.9+t20220409165627.tmeta.g83deed0.orig.tar.gz
 b1b366116d1338d598849498866124cc06f72bb3 1024 shellfu-bash-jat_0.2.9+t20220409165627.tmeta.g83deed0-1.debian.tar.xz
Checksums-Sha256:
 0185571ac3f6c33e34e3de47eecf2d43958c0ffc19f7f54fe22972db25aa73db 97847 shellfu-bash-jat_0.2.9+t20220409165627.tmeta.g83deed0.orig.tar.gz
 5b67833e0070222397069e45d7dbd2cb3e24874bfc60693f831c71bb959fc330 1024 shellfu-bash-jat_0.2.9+t20220409165627.tmeta.g83deed0-1.debian.tar.xz
Files:
 6349750dffdeb14e514c20ced7b27950 97847 shellfu-bash-jat_0.2.9+t20220409165627.tmeta.g83deed0.orig.tar.gz
 6ff6fb27c26cd80320f4296b5dd797ad 1024 shellfu-bash-jat_0.2.9+t20220409165627.tmeta.g83deed0-1.debian.tar.xz
