Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.20-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.11), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 9f985bf06183a78f84314f9228102ad203931f6a 69164 bmo_0.1.20.orig.tar.gz
 cb094b7a1dfa975b8a764881db256b76d2e4e4ff 1604 bmo_0.1.20-1.debian.tar.xz
Checksums-Sha256:
 418d6da66b9332e99c1563ed36cd4a74db8ce5a85fbe8fe2c0d02e6bb731862b 69164 bmo_0.1.20.orig.tar.gz
 f7bad6f1c48f36031df45e9375d89849018e99c66da5c88c03dd6876a0c167bd 1604 bmo_0.1.20-1.debian.tar.xz
Files:
 b1730b91aa43ad2b9f8da5c6ce832582 69164 bmo_0.1.20.orig.tar.gz
 2deb2c2bf38f983ef4d5d586cd96a856 1604 bmo_0.1.20-1.debian.tar.xz
