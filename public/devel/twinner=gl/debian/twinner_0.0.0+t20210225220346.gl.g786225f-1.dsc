Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.0+t20210225220346.gl.g786225f-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9)
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 2f5a77b7adf56aed68471a940771d507d72836df 31887 twinner_0.0.0+t20210225220346.gl.g786225f.orig.tar.gz
 d6307c3ab98422ccb708a5b074e31c9479eb4be7 804 twinner_0.0.0+t20210225220346.gl.g786225f-1.debian.tar.xz
Checksums-Sha256:
 92253578bd3805b21d10660cb4b7bb20b8eb7d2f6232e0229f496686dc59bd73 31887 twinner_0.0.0+t20210225220346.gl.g786225f.orig.tar.gz
 6188ca2805ee2be20c20133293f881d71a1b258094c3d23be21cb72fbe14f70e 804 twinner_0.0.0+t20210225220346.gl.g786225f-1.debian.tar.xz
Files:
 ca3a78b8d0f60e11f3d1228e6b19179b 31887 twinner_0.0.0+t20210225220346.gl.g786225f.orig.tar.gz
 fb63aa8ca94a5c7b26a6486c83b65628 804 twinner_0.0.0+t20210225220346.gl.g786225f-1.debian.tar.xz
