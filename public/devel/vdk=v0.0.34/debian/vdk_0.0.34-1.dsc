Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.34-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 282e8f0096b9046f7fda1ce1b778c6d15db97767 75954 vdk_0.0.34.orig.tar.gz
 f59f776b413a256cce0c0abb5205572c831c398e 1080 vdk_0.0.34-1.debian.tar.xz
Checksums-Sha256:
 624c939d97f5b9c9013cdcbe901534067a73b382cdb756e53ec81a79772ad79c 75954 vdk_0.0.34.orig.tar.gz
 ca8c36f3041262d967eb3e6de592320c178b8c8dc7e2ac3dbe410a74ed6b5723 1080 vdk_0.0.34-1.debian.tar.xz
Files:
 eab429dd91da313de2892a3c306d5924 75954 vdk_0.0.34.orig.tar.gz
 d50941d6f742affed36bcfb2c744402f 1080 vdk_0.0.34-1.debian.tar.xz
