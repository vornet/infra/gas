Format: 3.0 (quilt)
Source: shellfu-bash-uripecker
Binary: shellfu-bash-uripecker
Architecture: all
Version: 0.1.10+t20210225234235.slash.g77fdb30-1
Maintainer: Alois Mahdal <netvor+uripecker@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-uripecker
Build-Depends: debhelper (>= 9), make, python, libpython3.7-minimal, libpython3.7-stdlib, shellfu (>= 0.10.4), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-uripecker deb misc extra arch=all
Checksums-Sha1:
 b50e3b5c8e24b93f5e04181ff79c59aad8f0d386 46919 shellfu-bash-uripecker_0.1.10+t20210225234235.slash.g77fdb30.orig.tar.gz
 9d3271c5610bcf25daa0c313f8a6464390a5b28e 1096 shellfu-bash-uripecker_0.1.10+t20210225234235.slash.g77fdb30-1.debian.tar.xz
Checksums-Sha256:
 eea012f4250aed9487f17c42fb1e6e2575079ebc4e5f380f8edcc85eb7ab3dec 46919 shellfu-bash-uripecker_0.1.10+t20210225234235.slash.g77fdb30.orig.tar.gz
 febbe54c56e6320ef485a6b5f48d45f54bc1abd60c222aa39fdd1c7143ae5a83 1096 shellfu-bash-uripecker_0.1.10+t20210225234235.slash.g77fdb30-1.debian.tar.xz
Files:
 66b9dae58fbc581544512bdd2cf4da7a 46919 shellfu-bash-uripecker_0.1.10+t20210225234235.slash.g77fdb30.orig.tar.gz
 793882fedb9eb2ef6339d478c128bf50 1096 shellfu-bash-uripecker_0.1.10+t20210225234235.slash.g77fdb30-1.debian.tar.xz
