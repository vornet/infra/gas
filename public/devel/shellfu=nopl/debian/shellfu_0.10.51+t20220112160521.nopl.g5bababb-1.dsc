Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.51+t20220112160521.nopl.g5bababb-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 c6ed8953c38260d538a296c020c19685c9dca82b 93982 shellfu_0.10.51+t20220112160521.nopl.g5bababb.orig.tar.gz
 57407442998ad7c52350bcd6479f0129554f3f4e 2248 shellfu_0.10.51+t20220112160521.nopl.g5bababb-1.debian.tar.xz
Checksums-Sha256:
 92a86749cb9f9a420a6e24f959e918118ea74f517c59ef1e1be2df71b1ff63ac 93982 shellfu_0.10.51+t20220112160521.nopl.g5bababb.orig.tar.gz
 ba1e3a95e2bca0704dfa1aeb77975d25d71f10fa5a26d90e723e27b4bb6d576e 2248 shellfu_0.10.51+t20220112160521.nopl.g5bababb-1.debian.tar.xz
Files:
 e9616b3fa33702710aa449a9cb51a558 93982 shellfu_0.10.51+t20220112160521.nopl.g5bababb.orig.tar.gz
 782c5734342b5733137fe176a5132ab6 2248 shellfu_0.10.51+t20220112160521.nopl.g5bababb-1.debian.tar.xz
