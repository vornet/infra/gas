Format: 3.0 (quilt)
Source: jats-python-clapp
Binary: jats-python-clapp
Architecture: all
Version: 0.0.1-1
Maintainer: Alois Mahdal <netvor+clapp@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-python-clapp deb misc extra arch=all
Checksums-Sha1:
 f72bcfd67cb0d73ac2bc1471b2ab7b6581a1ede9 3016 jats-python-clapp_0.0.1.orig.tar.gz
 3fb7bf176f3e0160123d5da76e099a5d79389525 848 jats-python-clapp_0.0.1-1.debian.tar.xz
Checksums-Sha256:
 e862b166d18f5e8f6efb4cf4269fe34ba5fc25f3d74940b9a6c73527ce562fa9 3016 jats-python-clapp_0.0.1.orig.tar.gz
 dbb82d27d9ea904b3ae56cee9e9dd9cbd7702e0aea641911fe0c01c33e0d41b8 848 jats-python-clapp_0.0.1-1.debian.tar.xz
Files:
 6a25ebe52af752516c7d8bb13bdaaa7c 3016 jats-python-clapp_0.0.1.orig.tar.gz
 7e114c4b58b76ba34ec64d994f69a539 848 jats-python-clapp_0.0.1-1.debian.tar.xz
