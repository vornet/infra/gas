Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.18-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 7d42e832ae4c235313add29f8489c7c6a33e14d8 66064 vdk_0.0.18.orig.tar.gz
 d2209380e23633d06bc7c6fe4c1737991329ef01 1012 vdk_0.0.18-1.debian.tar.xz
Checksums-Sha256:
 474cf5e214805f5a092dfd9ae0ebac5251d2343f828aa5a146ce5c58438ed0b8 66064 vdk_0.0.18.orig.tar.gz
 5285d9b6d97b1852a8dd9576239f1e0791db5382d3f744956d93a83871e003f4 1012 vdk_0.0.18-1.debian.tar.xz
Files:
 2bc6ba38b67e8319c2ff4ba7952eeda2 66064 vdk_0.0.18.orig.tar.gz
 2af2a1d324f6a8d31309048cb25fc47e 1012 vdk_0.0.18-1.debian.tar.xz
