Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.2.1-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 f35089741e169b2722f7c3d80dc6c739f919d45e 88641 bmo_0.2.1.orig.tar.gz
 43271a662e5f77829af3d0207baef2b98df16fcb 1744 bmo_0.2.1-1.debian.tar.xz
Checksums-Sha256:
 93918e7dbfa0792d8a02919ff86d0a2a7ecc9ffbfffe74ba42f95bb0adab9076 88641 bmo_0.2.1.orig.tar.gz
 d22d289b93801228bb3653db42afab739e312385823a65dce55183ff9e9a9c23 1744 bmo_0.2.1-1.debian.tar.xz
Files:
 0cef13c8f60512c6f526bf44841a7c20 88641 bmo_0.2.1.orig.tar.gz
 05716cc24eabd6f514a867872e0b89e3 1744 bmo_0.2.1-1.debian.tar.xz
