Format: 3.0 (quilt)
Source: jats-demo
Binary: jats-demo
Architecture: all
Version: 0.1.4+t20210303102512.tmtize.gcc705cb-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-demo deb misc extra arch=all
Checksums-Sha1:
 582824130a16c75c69ac152d14967a5346f92bb5 6362 jats-demo_0.1.4+t20210303102512.tmtize.gcc705cb.orig.tar.gz
 0bcbe05f18b4b6b0b1a84651306e73ead9b30c69 836 jats-demo_0.1.4+t20210303102512.tmtize.gcc705cb-1.debian.tar.xz
Checksums-Sha256:
 70f48a258bb3b14cfa032930244bea3ea2ad4b4f5db538efb350b727f3204b15 6362 jats-demo_0.1.4+t20210303102512.tmtize.gcc705cb.orig.tar.gz
 7c63f08e107d2027726a707f728314161f5b002e7689a5490616892badeaaab2 836 jats-demo_0.1.4+t20210303102512.tmtize.gcc705cb-1.debian.tar.xz
Files:
 2f552ac4ddf2c4ff80e4d05f5ce5f371 6362 jats-demo_0.1.4+t20210303102512.tmtize.gcc705cb.orig.tar.gz
 fa9206fd6497ac0df9d1d4ff9b92d157 836 jats-demo_0.1.4+t20210303102512.tmtize.gcc705cb-1.debian.tar.xz
