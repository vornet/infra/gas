Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.22-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 3826742ce7ad3dd36877f979601c94026148411f 68936 vdk_0.0.22.orig.tar.gz
 98868010f62a5a09577f7f8c56882e049b2a9212 1020 vdk_0.0.22-1.debian.tar.xz
Checksums-Sha256:
 d1bcee99f5ad0f2e7fb0147b6f6c2622a84718450f38362ee3a89caca3770a28 68936 vdk_0.0.22.orig.tar.gz
 319ac94a3ac5c8d2fc54d2a914103f427bcab88a60fca391786e974130e707d4 1020 vdk_0.0.22-1.debian.tar.xz
Files:
 3388a4f66eefd16027f6985f3f97fde6 68936 vdk_0.0.22.orig.tar.gz
 ac2dd666e12b7cda3c77bc2cc701c6b4 1020 vdk_0.0.22-1.debian.tar.xz
