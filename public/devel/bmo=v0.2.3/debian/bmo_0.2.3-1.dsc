Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.2.3-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 a8104f2f19eb275cd38f81f7661262c27dfbd8c6 88417 bmo_0.2.3.orig.tar.gz
 b2cd41619bffa2e3409f873a9a9266987fdfa88c 1744 bmo_0.2.3-1.debian.tar.xz
Checksums-Sha256:
 f1311a791456b6c617392a996825eb3669654906f59306144532949be9182ca6 88417 bmo_0.2.3.orig.tar.gz
 f9b56cbd2a8658e8666e8dde4bb695af8a605f1e949cdad15259f9e9e6a9096b 1744 bmo_0.2.3-1.debian.tar.xz
Files:
 057a35c2a565a50aa3018dc591883346 88417 bmo_0.2.3.orig.tar.gz
 c07c2ad32aeca314485082de2b20f5e6 1744 bmo_0.2.3-1.debian.tar.xz
