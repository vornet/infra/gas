Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.5-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 036dd0092d6329c80f475c21808462b719c4c84a 68674 jattool_0.3.5.orig.tar.gz
 19d805b08d069b744b16d356719c3653646b4a35 1488 jattool_0.3.5-1.debian.tar.xz
Checksums-Sha256:
 080d0e9ae3101be64e97296f9ac191ee0fa4feb654d2c89adbd2e1f04117e37b 68674 jattool_0.3.5.orig.tar.gz
 24cdde062f0c74728f5bba14fb39a43de2a4e0e33dbf3eb7d667e66b246d4df9 1488 jattool_0.3.5-1.debian.tar.xz
Files:
 c1333924c8b35a7c939ab4319334a48c 68674 jattool_0.3.5.orig.tar.gz
 e178cad305024229a56ea3551df7ff9d 1488 jattool_0.3.5-1.debian.tar.xz
