Format: 3.0 (quilt)
Source: shellfu-sh-mdfmt
Binary: shellfu-sh-mdfmt
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+mdfmt@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-sh-mdfmt
Build-Depends: debhelper (>= 9), shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-sh
Package-List:
 shellfu-sh-mdfmt deb misc extra arch=all
Checksums-Sha1:
 d400ed95e4dc57ea26919431168ba1cbb1ed904b 50237 shellfu-sh-mdfmt_0.0.2.orig.tar.gz
 cab843a2fd3f1910a61b612aed3c524d705d5844 948 shellfu-sh-mdfmt_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 1d8b3ecd17c6e8759c6dbbe7a08734b9afd0eda25666009321917f7c43b60eaf 50237 shellfu-sh-mdfmt_0.0.2.orig.tar.gz
 64c01142c2632e218000e00475bbc21326ace226a804c81a741dd82feea295ef 948 shellfu-sh-mdfmt_0.0.2-1.debian.tar.xz
Files:
 f7a1495df74cb45f33b6c5aa4a9b608a 50237 shellfu-sh-mdfmt_0.0.2.orig.tar.gz
 dbe06583b13068927cc1ea0153d951be 948 shellfu-sh-mdfmt_0.0.2-1.debian.tar.xz
