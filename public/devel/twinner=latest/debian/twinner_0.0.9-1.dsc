Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.9-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 529166653d36906c5b400e12af1199c167e24c60 43421 twinner_0.0.9.orig.tar.gz
 b8d6c783ae3e66e179cf322f5753fd96bfce001f 812 twinner_0.0.9-1.debian.tar.xz
Checksums-Sha256:
 9da9acca2536cbbff14017c6d21244a43c953b18e78629bce9ef696105107241 43421 twinner_0.0.9.orig.tar.gz
 733d6146f88e1c30a76cdd294c6370bf84958ebcc24d077456c5a631bef93549 812 twinner_0.0.9-1.debian.tar.xz
Files:
 ae2b2f3e5574a1a7b56e210683a495ce 43421 twinner_0.0.9.orig.tar.gz
 6ab45fcbf1d9432c991e8b2c46ee9652 812 twinner_0.0.9-1.debian.tar.xz
