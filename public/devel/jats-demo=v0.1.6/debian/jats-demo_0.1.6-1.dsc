Format: 3.0 (quilt)
Source: jats-demo
Binary: jats-demo
Architecture: all
Version: 0.1.6-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-demo deb misc extra arch=all
Checksums-Sha1:
 0bbc3e7a295f701f920a6858ef78a0ea75413e1a 6089 jats-demo_0.1.6.orig.tar.gz
 ee97dc51af303a0660cfd59ca81385d2980c5a45 788 jats-demo_0.1.6-1.debian.tar.xz
Checksums-Sha256:
 1776e527cbe08e6bd5b0952fa82bab6433761bfbe1b13030e9b90aa727ad6f7d 6089 jats-demo_0.1.6.orig.tar.gz
 49bff3e7f97ebe983a1dc44a4562dcbe9ec7455eb3c9aed50499233b386f5686 788 jats-demo_0.1.6-1.debian.tar.xz
Files:
 da8774193e96158df5251cc880790915 6089 jats-demo_0.1.6.orig.tar.gz
 9e7a7db90fc9857d5c9c6256a638a1d3 788 jats-demo_0.1.6-1.debian.tar.xz
