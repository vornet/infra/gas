Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.61+t20230523215810.num.g36f2447-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 b4fb47d3087fc0dfbce68bb12437a2bb444fad45 94610 shellfu_0.10.61+t20230523215810.num.g36f2447.orig.tar.gz
 f590d26de9d1985c6e469faab4a7b4c0d9bde4d9 2244 shellfu_0.10.61+t20230523215810.num.g36f2447-1.debian.tar.xz
Checksums-Sha256:
 ba5dd0ac2885ad934983747a7c005372bc6cb68ccc5a3a558c32cc1b61b656fe 94610 shellfu_0.10.61+t20230523215810.num.g36f2447.orig.tar.gz
 cc021b2b611ee359d33c9a6fbc3432917d60a9bbdecbbfdf11ad3304b2fe7c3c 2244 shellfu_0.10.61+t20230523215810.num.g36f2447-1.debian.tar.xz
Files:
 bc8260e3df78862588681d15afbb38ea 94610 shellfu_0.10.61+t20230523215810.num.g36f2447.orig.tar.gz
 9753fd271c1cf4f9babeec5ad204735b 2244 shellfu_0.10.61+t20230523215810.num.g36f2447-1.debian.tar.xz
