Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.46-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 0dc3afb25e8c14241d2b4574e6504c2d1253063e 84913 shellfu_0.10.46.orig.tar.gz
 fddc670b664eacc02b56839b8f0d396234cae07f 2208 shellfu_0.10.46-1.debian.tar.xz
Checksums-Sha256:
 838c2f8862cf158b75d28b9284548580bb2478ebde1c791bacb9ca1e827708d0 84913 shellfu_0.10.46.orig.tar.gz
 fd16cc55d765ab9c02b78de6cc888007ffb11c901b608aa9717d6417dd647459 2208 shellfu_0.10.46-1.debian.tar.xz
Files:
 e1b2af9e3f6505e28ef9aede3ef81e48 84913 shellfu_0.10.46.orig.tar.gz
 9c570f741fb22a64a79d4e1ec6c3d04e 2208 shellfu_0.10.46-1.debian.tar.xz
