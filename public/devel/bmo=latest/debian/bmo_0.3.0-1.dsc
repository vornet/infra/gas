Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.3.0-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 5d09d82a4859532e72171ce831c3defe3770bdad 82781 bmo_0.3.0.orig.tar.gz
 65ea6222451124ada7481cdd6190cc5437a34dae 1696 bmo_0.3.0-1.debian.tar.xz
Checksums-Sha256:
 e63490a82f17fdac71e7ca5d070b0b037a377dbb869a6633c271cf1761a4336f 82781 bmo_0.3.0.orig.tar.gz
 872ba99c049a2483590b6d2f41696f543ab60b7939d2ecdd0771c302f0040af4 1696 bmo_0.3.0-1.debian.tar.xz
Files:
 1dfffc57d765f08d9d8af3f387f1f4af 82781 bmo_0.3.0.orig.tar.gz
 7572e5e3891aacd17ab74e6db6042f9a 1696 bmo_0.3.0-1.debian.tar.xz
