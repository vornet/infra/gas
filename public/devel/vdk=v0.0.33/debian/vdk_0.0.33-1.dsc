Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.33-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 9361c1cef90d93aeb4780ad0ea98345ce5707990 75914 vdk_0.0.33.orig.tar.gz
 9ffa578c4e29f5eceee0188aa366d278f250e3b7 1080 vdk_0.0.33-1.debian.tar.xz
Checksums-Sha256:
 4580faa1fd12e4972feeea329b15ed69ef1d64c39d6faececbea2c1df05d828c 75914 vdk_0.0.33.orig.tar.gz
 14ac8ba2ca7a506f827465dbc8fa44edd2b4984f1e3fb3b41b00604e3ebe881b 1080 vdk_0.0.33-1.debian.tar.xz
Files:
 5e8a43abb828c28ee9181070529fcdd9 75914 vdk_0.0.33.orig.tar.gz
 99db97e44528c45f5c716ac9a86eab97 1080 vdk_0.0.33-1.debian.tar.xz
