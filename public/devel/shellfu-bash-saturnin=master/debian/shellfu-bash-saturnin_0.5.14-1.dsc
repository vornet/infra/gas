Format: 3.0 (quilt)
Source: shellfu-bash-saturnin
Binary: shellfu-bash-saturnin
Architecture: all
Version: 0.5.14-1
Maintainer: Alois Mahdal <netvor+saturnin@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-saturnin
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-inigrep, shellfu-bash-pretty, shellfu-sh-exit
Package-List:
 shellfu-bash-saturnin deb misc extra arch=all
Checksums-Sha1:
 dd304a9d38557486df7ddaf5f7f27408fb3a71ea 59130 shellfu-bash-saturnin_0.5.14.orig.tar.gz
 2cff6eea2fc37bde7bf4cc02301ea6d0956e88be 1096 shellfu-bash-saturnin_0.5.14-1.debian.tar.xz
Checksums-Sha256:
 e09a46800a86fd15143d06b76072e6054239d11827ff88a9978144eb5af9f7a7 59130 shellfu-bash-saturnin_0.5.14.orig.tar.gz
 f7cf9d843cd7d2bc95d08cb35c387d07122d6b390bd0da4853eb57dc680bb48d 1096 shellfu-bash-saturnin_0.5.14-1.debian.tar.xz
Files:
 d80f2f51661f5dddb9317368d1d58003 59130 shellfu-bash-saturnin_0.5.14.orig.tar.gz
 a04f2491830cab54da75cea2160fed8f 1096 shellfu-bash-saturnin_0.5.14-1.debian.tar.xz
