Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.10-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 d32289eb12182151dfdf90800fa96e6de71ceb29 97464 shellfu-bash-jat_0.2.10.orig.tar.gz
 4fd14b0adcda92d00e646a9cbc54af5d67187feb 1004 shellfu-bash-jat_0.2.10-1.debian.tar.xz
Checksums-Sha256:
 6f120a038babc7bffc0c7d0c3b650dbc628b875f39c9471eb52f29080daa5f5a 97464 shellfu-bash-jat_0.2.10.orig.tar.gz
 d9f8764354d66dfdca0e0dc5eb38b9ac2405c41958cbe9105c8dfd5a5121f964 1004 shellfu-bash-jat_0.2.10-1.debian.tar.xz
Files:
 5d8dd90f35622b61fa581bab0882b7ec 97464 shellfu-bash-jat_0.2.10.orig.tar.gz
 118c71453f353b3186a875aae67739c2 1004 shellfu-bash-jat_0.2.10-1.debian.tar.xz
