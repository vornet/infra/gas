Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.51-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 29274d6cf90106094f845c3bcac97727511728e3 93068 shellfu_0.10.51.orig.tar.gz
 93fca5911cbdb47e20bd3c961bf131a8d5ebf3f8 2224 shellfu_0.10.51-1.debian.tar.xz
Checksums-Sha256:
 882f255a714cd15d0c68e8275c133ac2a2a58555652c1e7a30d5fe7557a8fcf6 93068 shellfu_0.10.51.orig.tar.gz
 5152a2569ed2e56262d07f5ace8966e3678a72463be1659eadcbd3fa4f1c04c0 2224 shellfu_0.10.51-1.debian.tar.xz
Files:
 720018a035e5a940f2ef3cfb4bca2de8 93068 shellfu_0.10.51.orig.tar.gz
 5723694b663042ebaa6854ce3e7f5099 2224 shellfu_0.10.51-1.debian.tar.xz
