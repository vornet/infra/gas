Format: 3.0 (quilt)
Source: shellfu-bash-dist
Binary: shellfu-bash-dist
Architecture: all
Version: 0.2.5-1
Maintainer: Alois Mahdal <netvor+dist@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-dist
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-dist deb misc extra arch=all
Checksums-Sha1:
 0246a905fe01bb3cd1c087f7e3a68eaa2f65548f 47606 shellfu-bash-dist_0.2.5.orig.tar.gz
 edf1b2121d5584f61bdbc7f1f3f776e65beffc3c 860 shellfu-bash-dist_0.2.5-1.debian.tar.xz
Checksums-Sha256:
 c9202c0d329bf4c7faafde28b5edcf1133b44d70cd73f51acba8b48c4db55071 47606 shellfu-bash-dist_0.2.5.orig.tar.gz
 ce44e278b6e38aee670bc0814513e9fdb221b5d5bb0c1e7f11da89edb09c0ccd 860 shellfu-bash-dist_0.2.5-1.debian.tar.xz
Files:
 c8ed17b17f7d2dc2e83c888473aefa32 47606 shellfu-bash-dist_0.2.5.orig.tar.gz
 640b6289372af1be12d1d9b95b648d4d 860 shellfu-bash-dist_0.2.5-1.debian.tar.xz
