Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 ef4bc3a05f5139a40cf5f814bdab0ddc845b8fb9 34012 shellfu-bash-bkrh_0.0.2.orig.tar.gz
 e3fcca7da0ee2a3fc964322bd01b531bad16ba02 816 shellfu-bash-bkrh_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 ce7748567263432ee0c03486503394d19c24504da85c8094f5d4cccdd4b45179 34012 shellfu-bash-bkrh_0.0.2.orig.tar.gz
 b00431ba032f68549f840d89cb0280761a73767f76047391b1786405afb6a4e9 816 shellfu-bash-bkrh_0.0.2-1.debian.tar.xz
Files:
 99366163db3fa99708a990fe65583144 34012 shellfu-bash-bkrh_0.0.2.orig.tar.gz
 504441441ba83bab85d9ebd177797476 816 shellfu-bash-bkrh_0.0.2-1.debian.tar.xz
