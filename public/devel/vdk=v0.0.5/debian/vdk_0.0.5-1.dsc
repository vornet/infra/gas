Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper (>= 9), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 a55c58aadd5a14d720de8d0656873086abbcdc02 60952 vdk_0.0.5.orig.tar.gz
 c30add27a158c8a6f623185dffc431906720ba2e 992 vdk_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 658942067b7c06d0af0438ef6d798218784d2ec52d5b3210d7518c49c3868608 60952 vdk_0.0.5.orig.tar.gz
 73c38f2b25956d67b3b14ea9170a2d20599e4d58e89d112a4a57f477001e59cd 992 vdk_0.0.5-1.debian.tar.xz
Files:
 5df45e4c7e4de78b20ebf030021b7c4e 60952 vdk_0.0.5.orig.tar.gz
 38bff8a88dd0e66216e858c1a61646bb 992 vdk_0.0.5-1.debian.tar.xz
