Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.2.4-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 59908c27a5e7e0408a4f4412f52a2219ef2966d0 82143 bmo_0.2.4.orig.tar.gz
 3e1df82c46657fd41f4a65a14d5f4b5d9473f089 1696 bmo_0.2.4-1.debian.tar.xz
Checksums-Sha256:
 a2d5778fc4478aee04adae20451ef4e2a63442a86f3274f91469037786281d3f 82143 bmo_0.2.4.orig.tar.gz
 cd516ef16a5fe556c4e3e83f68c4585400d234805a27b8c75210b068c6b28fb9 1696 bmo_0.2.4-1.debian.tar.xz
Files:
 23de046e0ba8a347e321162a42aaca58 82143 bmo_0.2.4.orig.tar.gz
 22f61bea3072915258b48f05715a205a 1696 bmo_0.2.4-1.debian.tar.xz
