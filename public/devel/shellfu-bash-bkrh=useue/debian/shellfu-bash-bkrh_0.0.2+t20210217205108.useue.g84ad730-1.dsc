Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.2+t20210217205108.useue.g84ad730-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 2810564bc0d9cef5b825ccd33e4890b4151f3d97 34085 shellfu-bash-bkrh_0.0.2+t20210217205108.useue.g84ad730.orig.tar.gz
 fddc487736833744f4927cc2c9cf8b51ba87b9b8 836 shellfu-bash-bkrh_0.0.2+t20210217205108.useue.g84ad730-1.debian.tar.xz
Checksums-Sha256:
 914cf97e6ba0df6cd77db7874bdb9ad1e672cbf82d8e657a747372cacf2ee3ba 34085 shellfu-bash-bkrh_0.0.2+t20210217205108.useue.g84ad730.orig.tar.gz
 2f6792771aa0160a4b30feedde33e7cd899a3967014290e4e15aa04f3ca5786c 836 shellfu-bash-bkrh_0.0.2+t20210217205108.useue.g84ad730-1.debian.tar.xz
Files:
 3af3c5cfa644bd5576802419f39fe4bc 34085 shellfu-bash-bkrh_0.0.2+t20210217205108.useue.g84ad730.orig.tar.gz
 3ccc9d131303d187176585b3f7a5d5c4 836 shellfu-bash-bkrh_0.0.2+t20210217205108.useue.g84ad730-1.debian.tar.xz
