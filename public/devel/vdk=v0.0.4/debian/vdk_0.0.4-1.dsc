Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.4-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper (>= 9), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 fef758812fb84c3bd5b676431a045d7557bdee55 60935 vdk_0.0.4.orig.tar.gz
 e8e83855b1370f1106fe12f197a7124601daced0 992 vdk_0.0.4-1.debian.tar.xz
Checksums-Sha256:
 f6ac4ce2ed006a223cd5e4009f3234314f64fa0f8873a195faadbc4dd4c6b0ec 60935 vdk_0.0.4.orig.tar.gz
 8a9748c855dbe74cf0bf67756f379d4c53d5d7e61e503b602542eb8546007590 992 vdk_0.0.4-1.debian.tar.xz
Files:
 db4ae30a8298dad7116e0917064cb15b 60935 vdk_0.0.4.orig.tar.gz
 6f03dfdf0ccf9d771a8de414c38cf455 992 vdk_0.0.4-1.debian.tar.xz
