Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.9-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 c76264f795891fd21d2272cad7c3bdf87fba8f4c 43429 twinner_0.0.9.orig.tar.gz
 b8d6c783ae3e66e179cf322f5753fd96bfce001f 812 twinner_0.0.9-1.debian.tar.xz
Checksums-Sha256:
 a333cf95ef7326265d7b3988e48b0e3043cbe56cc07b0f6ec164149d836db429 43429 twinner_0.0.9.orig.tar.gz
 733d6146f88e1c30a76cdd294c6370bf84958ebcc24d077456c5a631bef93549 812 twinner_0.0.9-1.debian.tar.xz
Files:
 8cc5ff61c2bd763f3af95873dee89489 43429 twinner_0.0.9.orig.tar.gz
 6ab45fcbf1d9432c991e8b2c46ee9652 812 twinner_0.0.9-1.debian.tar.xz
