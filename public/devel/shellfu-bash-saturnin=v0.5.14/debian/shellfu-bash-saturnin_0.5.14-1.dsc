Format: 3.0 (quilt)
Source: shellfu-bash-saturnin
Binary: shellfu-bash-saturnin
Architecture: all
Version: 0.5.14-1
Maintainer: Alois Mahdal <netvor+saturnin@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-saturnin
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-inigrep, shellfu-bash-pretty, shellfu-sh-exit
Package-List:
 shellfu-bash-saturnin deb misc extra arch=all
Checksums-Sha1:
 da9483e06ab1393e33c73c46ea7c361c2c05a83f 59164 shellfu-bash-saturnin_0.5.14.orig.tar.gz
 2cff6eea2fc37bde7bf4cc02301ea6d0956e88be 1096 shellfu-bash-saturnin_0.5.14-1.debian.tar.xz
Checksums-Sha256:
 1b91a4096bdc49a11a481a0fc59f79e95f8e9a66422233545f9569b58eb06b50 59164 shellfu-bash-saturnin_0.5.14.orig.tar.gz
 f7cf9d843cd7d2bc95d08cb35c387d07122d6b390bd0da4853eb57dc680bb48d 1096 shellfu-bash-saturnin_0.5.14-1.debian.tar.xz
Files:
 bd0a215a084036369e6f28de230e209f 59164 shellfu-bash-saturnin_0.5.14.orig.tar.gz
 a04f2491830cab54da75cea2160fed8f 1096 shellfu-bash-saturnin_0.5.14-1.debian.tar.xz
