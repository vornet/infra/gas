Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.62-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 52c15ce8adfe9d5b9b59667dab67f97568a50aee 93906 shellfu_0.10.62.orig.tar.gz
 f917a8b0867f77fa497a964ec11a85a0b3e2a3d1 2228 shellfu_0.10.62-1.debian.tar.xz
Checksums-Sha256:
 cfc1afc922819ac4b729b88af9b0aa4e3ab8486d9f9557f03310c37aaca401fe 93906 shellfu_0.10.62.orig.tar.gz
 89d43f124af9b248fb1af053959127ad5e7666135715c37c00ea089bf1da15fa 2228 shellfu_0.10.62-1.debian.tar.xz
Files:
 adf60e686dddb3030c576b361c2081ab 93906 shellfu_0.10.62.orig.tar.gz
 bae02e0e91e4a952b212ed0684414720 2228 shellfu_0.10.62-1.debian.tar.xz
