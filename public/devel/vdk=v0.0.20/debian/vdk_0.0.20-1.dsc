Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.20-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 2126bff38fd98b784e6cd38dd8256d7e7a1b67b9 67180 vdk_0.0.20.orig.tar.gz
 4806e5c048cd768cf9a3c771f79d16e8f019872d 1012 vdk_0.0.20-1.debian.tar.xz
Checksums-Sha256:
 2dcb8395840d8f70d93f1c58fd42edfc2d08aa4849692dc0d7ede2e5bfb4e087 67180 vdk_0.0.20.orig.tar.gz
 c29e649667c8de8e0841a06d79d3971599c8a8057ffd3dd9748f403ed4fac7a5 1012 vdk_0.0.20-1.debian.tar.xz
Files:
 33a62b39adf3767a4f61ebd08335695e 67180 vdk_0.0.20.orig.tar.gz
 1f85cdd05b536f9bb08abea1d0601548 1012 vdk_0.0.20-1.debian.tar.xz
