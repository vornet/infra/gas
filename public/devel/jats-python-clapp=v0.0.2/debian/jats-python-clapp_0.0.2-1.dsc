Format: 3.0 (quilt)
Source: jats-python-clapp
Binary: jats-python-clapp
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+clapp@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-python-clapp deb misc extra arch=all
Checksums-Sha1:
 a84b3c11d0a0e0f7ee7ef78f3bc8974d46009809 3013 jats-python-clapp_0.0.2.orig.tar.gz
 d3a80654076238669603cab86027ad66c53fd5ba 848 jats-python-clapp_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 3f7aac86e5922cb997a0fe2f437974d39474eddcda63d2464a5da3fc2ed21329 3013 jats-python-clapp_0.0.2.orig.tar.gz
 2e47b1cdcbbefd3adfc67acd2fdeb8a833df38f17db1034aa506268d7b8d352b 848 jats-python-clapp_0.0.2-1.debian.tar.xz
Files:
 3da57a7aac3e50167182459cbd032815 3013 jats-python-clapp_0.0.2.orig.tar.gz
 486147508c7de1b1d64d50ac85a399b8 848 jats-python-clapp_0.0.2-1.debian.tar.xz
