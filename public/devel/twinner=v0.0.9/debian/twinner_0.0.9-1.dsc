Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.9-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 82f34f19f1cfce618cda5abf80c2b352c5e606c2 43426 twinner_0.0.9.orig.tar.gz
 b8d6c783ae3e66e179cf322f5753fd96bfce001f 812 twinner_0.0.9-1.debian.tar.xz
Checksums-Sha256:
 bbe0e46b1936dbf611912015b0fae85c70723976e67dd04ee5a889582a87ad0c 43426 twinner_0.0.9.orig.tar.gz
 733d6146f88e1c30a76cdd294c6370bf84958ebcc24d077456c5a631bef93549 812 twinner_0.0.9-1.debian.tar.xz
Files:
 a4521bbc256f069b4a428e3ba1c1b6d1 43426 twinner_0.0.9.orig.tar.gz
 6ab45fcbf1d9432c991e8b2c46ee9652 812 twinner_0.0.9-1.debian.tar.xz
