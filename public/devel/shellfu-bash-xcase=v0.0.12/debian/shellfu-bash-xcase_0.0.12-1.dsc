Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.12-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 66be05f2e0c20f21dff53c52acc07f5e3e4ea081 44913 shellfu-bash-xcase_0.0.12.orig.tar.gz
 218f98668f1a6876f5a450c840072ac04c2be0eb 1016 shellfu-bash-xcase_0.0.12-1.debian.tar.xz
Checksums-Sha256:
 1c8d72014b09bce60bfc16f4f6901985f1d7914b5305ca7537f41c0cbaf79199 44913 shellfu-bash-xcase_0.0.12.orig.tar.gz
 a73257be041fe1974293fe5bd518e0845e48a44295d2507d8ba31f1803a62f54 1016 shellfu-bash-xcase_0.0.12-1.debian.tar.xz
Files:
 0e8d637b9ccfa4fd5b27751c90c319ee 44913 shellfu-bash-xcase_0.0.12.orig.tar.gz
 cd8785260e5c96aa7be7919ad08fb125 1016 shellfu-bash-xcase_0.0.12-1.debian.tar.xz
