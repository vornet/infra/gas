Format: 3.0 (quilt)
Source: shellfu-bash-saturnin
Binary: shellfu-bash-saturnin
Architecture: all
Version: 0.5.10-1
Maintainer: Alois Mahdal <netvor+saturnin@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-saturnin
Build-Depends: debhelper (>= 9), shellfu (>= 0.10.24), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-inigrep, shellfu-bash-pretty, shellfu-sh-exit
Package-List:
 shellfu-bash-saturnin deb misc extra arch=all
Checksums-Sha1:
 78530c278ac161877df036a21fe6f0e586e87d66 55144 shellfu-bash-saturnin_0.5.10.orig.tar.gz
 788da74d4d85560b886244f98f6910c82a61d2bd 1064 shellfu-bash-saturnin_0.5.10-1.debian.tar.xz
Checksums-Sha256:
 306e680543a50d0fb8aae15641229e224af4cd71fa86eca1d58217889dc35f48 55144 shellfu-bash-saturnin_0.5.10.orig.tar.gz
 fdf88e23e7a9d056c92f40f16004aef89c1773fc336f184fe85ad813fea783fb 1064 shellfu-bash-saturnin_0.5.10-1.debian.tar.xz
Files:
 c582b2844adbd3acff06e84540260b4f 55144 shellfu-bash-saturnin_0.5.10.orig.tar.gz
 70b60151b948296705ed31e158ecd130 1064 shellfu-bash-saturnin_0.5.10-1.debian.tar.xz
