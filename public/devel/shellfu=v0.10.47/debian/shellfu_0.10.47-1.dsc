Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.47-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 36bc4e0755546ef284d55d9f1c64836a6261252c 85734 shellfu_0.10.47.orig.tar.gz
 5333b587ff9ee4e75c7f4206c037f40d4ccdabb8 2208 shellfu_0.10.47-1.debian.tar.xz
Checksums-Sha256:
 929044e3ac8c5a64d2ed93bfb5237ab7b16432da04b796ad678e83b94546ccd3 85734 shellfu_0.10.47.orig.tar.gz
 b3a5c7f81fd7dea4cffe35e6c515b84e1cc87b93b5fdf250b2ad2e43cefb8b42 2208 shellfu_0.10.47-1.debian.tar.xz
Files:
 6557dada42b15ad5423c78e3ea1f50a6 85734 shellfu_0.10.47.orig.tar.gz
 6399f87f49c513fa849e8e0b73d45f03 2208 shellfu_0.10.47-1.debian.tar.xz
