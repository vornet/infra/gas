Format: 3.0 (quilt)
Source: shellfu-bash-uripecker
Binary: shellfu-bash-uripecker
Architecture: all
Version: 0.1.11-1
Maintainer: Alois Mahdal <netvor+uripecker@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-uripecker
Build-Depends: debhelper (>= 9), make, python, libpython3.7-minimal, libpython3.7-stdlib, shellfu (>= 0.10.4), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-uripecker deb misc extra arch=all
Checksums-Sha1:
 f35df602d8d8e4bf0987e8b608201cc0baab05be 46486 shellfu-bash-uripecker_0.1.11.orig.tar.gz
 07c659d8f728c84b44284312f2a358e57d57192b 1076 shellfu-bash-uripecker_0.1.11-1.debian.tar.xz
Checksums-Sha256:
 ad674ab50f3b9516e11b0ea761b3ac9b0d23a4890896d0a03b42ba8ecbf7fb22 46486 shellfu-bash-uripecker_0.1.11.orig.tar.gz
 5cf96ec013a5bab11369a020a04ca9eae8765e4a13780e5733bf454f8c3db141 1076 shellfu-bash-uripecker_0.1.11-1.debian.tar.xz
Files:
 9e49e8467c7c300165a7c45798440a06 46486 shellfu-bash-uripecker_0.1.11.orig.tar.gz
 8bfe6d3ebebfc2b0178170ac240ae6fd 1076 shellfu-bash-uripecker_0.1.11-1.debian.tar.xz
