Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.19-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.11), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0), xclip
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 f644366f786f197d46edd95af018d186c88adb10 69164 bmo_0.1.19.orig.tar.gz
 f0763d408d64553538be1573a9a9f5c157632aa9 1608 bmo_0.1.19-1.debian.tar.xz
Checksums-Sha256:
 b784996bc0290afc6909e78c450e997e2cccbbadd950b8efd7d3c376be568ab8 69164 bmo_0.1.19.orig.tar.gz
 845e3364a7398fdf415944e8b26fd13f53d410184f54b4eed893fdff2bce4661 1608 bmo_0.1.19-1.debian.tar.xz
Files:
 a56e1de681392c2a1a1672e0d45433fd 69164 bmo_0.1.19.orig.tar.gz
 6d838bfd9b9f750e40eec543ca4b3658 1608 bmo_0.1.19-1.debian.tar.xz
