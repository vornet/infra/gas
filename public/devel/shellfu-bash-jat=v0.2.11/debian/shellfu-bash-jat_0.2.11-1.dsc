Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.11-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 5e994e70d439b4ee4e055eb973d10b4670970fc2 101636 shellfu-bash-jat_0.2.11.orig.tar.gz
 4a1f4f47791b2f48b92ad5b34fbf1bcc588a3884 1004 shellfu-bash-jat_0.2.11-1.debian.tar.xz
Checksums-Sha256:
 b7c20ad498ec9a492a93999e8ae0145120ff823482eeae9d775c79e86942e64b 101636 shellfu-bash-jat_0.2.11.orig.tar.gz
 6dcc5a06e2a7d1b4fc00729923f5b9e102ac6403432a6b9635475df2dc822b41 1004 shellfu-bash-jat_0.2.11-1.debian.tar.xz
Files:
 ba54eb13cc695212172f090b66c018b3 101636 shellfu-bash-jat_0.2.11.orig.tar.gz
 d4910b2933e6daf32aece6a39812ccfd 1004 shellfu-bash-jat_0.2.11-1.debian.tar.xz
