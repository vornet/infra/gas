Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.10+t20220411182635.dev.g531cc0f-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 acd51ae3a96fcc27b67cae0a6c25634d5cb8aadb 101794 shellfu-bash-jat_0.2.10+t20220411182635.dev.g531cc0f.orig.tar.gz
 9092119aae196869736c57702d73aa21ba86a084 1024 shellfu-bash-jat_0.2.10+t20220411182635.dev.g531cc0f-1.debian.tar.xz
Checksums-Sha256:
 a90bcecab0078bd3d714faed24c6f0ddf60e9e7ceb0889ae85644b9da33d9d4a 101794 shellfu-bash-jat_0.2.10+t20220411182635.dev.g531cc0f.orig.tar.gz
 e8419fc976278b98ec3207dc161d51cfaa1e6c2e5520c548490967f2fdf4f18b 1024 shellfu-bash-jat_0.2.10+t20220411182635.dev.g531cc0f-1.debian.tar.xz
Files:
 4cedc09ecfbdae29a63e100720326a11 101794 shellfu-bash-jat_0.2.10+t20220411182635.dev.g531cc0f.orig.tar.gz
 626cb589ebb8341a074ef998f9a24401 1024 shellfu-bash-jat_0.2.10+t20220411182635.dev.g531cc0f-1.debian.tar.xz
