Format: 3.0 (quilt)
Source: jats-demo
Binary: jats-demo
Architecture: all
Version: 0.1.5-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-demo deb misc extra arch=all
Checksums-Sha1:
 e0957980e64fbfa6bc7102c591fccfed13b6e959 6116 jats-demo_0.1.5.orig.tar.gz
 2ff7f0c6d765dfd32a692738dbaefd7ba7bf5432 812 jats-demo_0.1.5-1.debian.tar.xz
Checksums-Sha256:
 988f01df20e32ad9da2816bfd823836ca585a7bc44307dcf39a6d26035f37d29 6116 jats-demo_0.1.5.orig.tar.gz
 b8213b13122137a46e131235458c87e0b1e8d582a2f35248ab509ae7d2bcaae3 812 jats-demo_0.1.5-1.debian.tar.xz
Files:
 6bb0044929eec3f4b1769afeec561367 6116 jats-demo_0.1.5.orig.tar.gz
 004c725f7b757531642c2c7f070d90a8 812 jats-demo_0.1.5-1.debian.tar.xz
