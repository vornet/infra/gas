Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.6+t20210426145707.logf.gd7070fe-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 c1eb8781e7cf71f522d53cdf9913ffc50f968240 83691 shellfu-bash-jat_0.2.6+t20210426145707.logf.gd7070fe.orig.tar.gz
 386c06bedb8c235c7c4e068df4e527f3689c054b 1024 shellfu-bash-jat_0.2.6+t20210426145707.logf.gd7070fe-1.debian.tar.xz
Checksums-Sha256:
 28f76129f377456347aa002afc5dcecdfe69181a705c16a65c167f7c00404927 83691 shellfu-bash-jat_0.2.6+t20210426145707.logf.gd7070fe.orig.tar.gz
 8d4ee3214d3cedd4239bf816b80acb0f9a8d14a9193774547f91c8495c4e6e61 1024 shellfu-bash-jat_0.2.6+t20210426145707.logf.gd7070fe-1.debian.tar.xz
Files:
 926f1be928cb31d6c33c7f21888bd86f 83691 shellfu-bash-jat_0.2.6+t20210426145707.logf.gd7070fe.orig.tar.gz
 ddcb3c809dbaee7a09aaa33205717ce3 1024 shellfu-bash-jat_0.2.6+t20210426145707.logf.gd7070fe-1.debian.tar.xz
