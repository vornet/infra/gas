Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.8-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 5695dc7d8756bad62fa08db3262916a4af043f94 71193 jattool_0.3.8.orig.tar.gz
 e74dd8254270289fc63b34491b6b2df48e203617 1492 jattool_0.3.8-1.debian.tar.xz
Checksums-Sha256:
 43e59b09f6378f6c804fd9ec1f5d5275fc5d0db8e3af2d9ce6cab4731914227a 71193 jattool_0.3.8.orig.tar.gz
 cd2f667f92aac2048163a37a95bb42084011e3f3e819f478c8074396916a0297 1492 jattool_0.3.8-1.debian.tar.xz
Files:
 7dd25d40b7355655a2dca5fea19ff159 71193 jattool_0.3.8.orig.tar.gz
 f5ecdaf8530c326d510e93f3b4559507 1492 jattool_0.3.8-1.debian.tar.xz
