Format: 3.0 (quilt)
Source: shellfu-bash-yaml
Binary: shellfu-bash-yaml
Architecture: all
Version: 0.0.0+t20210312112135.init.ge1c7e8f-1
Maintainer: Alois Mahdal <netvor+yaml@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-yaml
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-yaml deb misc extra arch=all
Checksums-Sha1:
 6779fb36a4717ca11146af9acaefb42fc8063a19 32866 shellfu-bash-yaml_0.0.0+t20210312112135.init.ge1c7e8f.orig.tar.gz
 79906b666747fea9fa720a187026e4e7d3e40953 848 shellfu-bash-yaml_0.0.0+t20210312112135.init.ge1c7e8f-1.debian.tar.xz
Checksums-Sha256:
 e92d80431ae244c3e8a15bb3a28af01c7d2d716052842975daaa4e90a924e50e 32866 shellfu-bash-yaml_0.0.0+t20210312112135.init.ge1c7e8f.orig.tar.gz
 228919559d53f226cad1eb1a875c37a7d1dc5a862f3073262445d319cbfba3b4 848 shellfu-bash-yaml_0.0.0+t20210312112135.init.ge1c7e8f-1.debian.tar.xz
Files:
 17fe2a1765c13238fd30fee9a90ee508 32866 shellfu-bash-yaml_0.0.0+t20210312112135.init.ge1c7e8f.orig.tar.gz
 d50b40fbafbb1cb422606c4e56038ecb 848 shellfu-bash-yaml_0.0.0+t20210312112135.init.ge1c7e8f-1.debian.tar.xz
