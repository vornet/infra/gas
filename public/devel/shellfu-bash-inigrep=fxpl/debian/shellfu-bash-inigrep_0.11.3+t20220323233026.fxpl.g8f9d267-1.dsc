Format: 3.0 (quilt)
Source: shellfu-bash-inigrep
Binary: shellfu-bash-inigrep
Architecture: all
Version: 0.11.3+t20220323233026.fxpl.g8f9d267-1
Maintainer: Alois Mahdal <netvor+inigrep@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-inigrep
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-inigrep deb misc extra arch=all
Checksums-Sha1:
 ce030e1fd93ae4027530dc9de259419d1910832a 78481 shellfu-bash-inigrep_0.11.3+t20220323233026.fxpl.g8f9d267.orig.tar.gz
 4dd9442d476ccf0447a243eec95037a3cde491cb 948 shellfu-bash-inigrep_0.11.3+t20220323233026.fxpl.g8f9d267-1.debian.tar.xz
Checksums-Sha256:
 39991a8baf0902a60ae95023a4312fefd7a69e9a922aa4f366e3f4bde49ec149 78481 shellfu-bash-inigrep_0.11.3+t20220323233026.fxpl.g8f9d267.orig.tar.gz
 9e063566a6c4d355e68f6be2a212304796eedfc29c1ecf3f7ac2af6a2caf7696 948 shellfu-bash-inigrep_0.11.3+t20220323233026.fxpl.g8f9d267-1.debian.tar.xz
Files:
 388fff2c2d1d0c18ebe4393fea8a4a78 78481 shellfu-bash-inigrep_0.11.3+t20220323233026.fxpl.g8f9d267.orig.tar.gz
 8c7649b18771eb5a023c55c681dc5691 948 shellfu-bash-inigrep_0.11.3+t20220323233026.fxpl.g8f9d267-1.debian.tar.xz
