Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.11-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 f9056fb89e61187e33e2891095bd9ed02c45b947 45201 shellfu-bash-xcase_0.0.11.orig.tar.gz
 f5c2e20f7b09eb143b7184d579d65b99f6cafba6 1012 shellfu-bash-xcase_0.0.11-1.debian.tar.xz
Checksums-Sha256:
 a17e822bb895b27a37f37a08d2130110a08511740e400b41947b0bc84d8c5383 45201 shellfu-bash-xcase_0.0.11.orig.tar.gz
 aa69d6eb9ba0df7838f98e35c918b87b99e34bc1888b090c73a2d71e75f28b92 1012 shellfu-bash-xcase_0.0.11-1.debian.tar.xz
Files:
 0afe3d13c79300405183d030531ca05d 45201 shellfu-bash-xcase_0.0.11.orig.tar.gz
 4478ff7f99cfa57c071eaf95cb6d8520 1012 shellfu-bash-xcase_0.0.11-1.debian.tar.xz
