Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-common, vdk-pylib
Architecture: all
Version: 0.2.3-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 861f9308f9aadf7200db0b6ffa347840faed72be 88258 vdk_0.2.3.orig.tar.gz
 7ab14e9cf496a6c4f0f1964828537349c9c75446 1144 vdk_0.2.3-1.debian.tar.xz
Checksums-Sha256:
 ed9fb4f0632e0bdfb22f46d5eda0917b2d48a0377d2dc9228d4b357962a1daec 88258 vdk_0.2.3.orig.tar.gz
 ff93a8f4d7047593f637d9d86446ace715c8a68b08dace5ea60c5f474aab9fdf 1144 vdk_0.2.3-1.debian.tar.xz
Files:
 2b43f296ce61b175076de669aa2cc0be 88258 vdk_0.2.3.orig.tar.gz
 c8f61f4b923b631d99a0c7cb3a02fc87 1144 vdk_0.2.3-1.debian.tar.xz
