Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 bc7fcf54491b1bf149afada08a8d2dc25dadceae 33637 twinner_0.0.5.orig.tar.gz
 eab3ae0ae63aa6e319c09a2074368069f5634b21 796 twinner_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 635308e00cf78b76e9a6dc4e5e7b262605ea709351317ff4492bad901fee53c4 33637 twinner_0.0.5.orig.tar.gz
 27b2c2470f1a23547e3b54311402a23d49a965f7877fc2a59aed34a087b3fff2 796 twinner_0.0.5-1.debian.tar.xz
Files:
 e5933880f2eabd72767dea9c35228277 33637 twinner_0.0.5.orig.tar.gz
 cc55409117def5affe21e82c18511eaf 796 twinner_0.0.5-1.debian.tar.xz
