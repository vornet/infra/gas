Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.2.3+t20240628151826.dev.gb4adfbb-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 d0deee75465a52c3af65a526fa6bdc6db98910bd 82246 bmo_0.2.3+t20240628151826.dev.gb4adfbb.orig.tar.gz
 e376cb070f9153593a8d7b507b8f62401edc6183 1724 bmo_0.2.3+t20240628151826.dev.gb4adfbb-1.debian.tar.xz
Checksums-Sha256:
 0c9c1f0f10eaf451b97dc1c2bee90c709de98f94242913754d546d6075f41532 82246 bmo_0.2.3+t20240628151826.dev.gb4adfbb.orig.tar.gz
 c4ba36aeb8fa53b1683563b9ca257c9cba27e9551169cddcb49160f42f5f6a60 1724 bmo_0.2.3+t20240628151826.dev.gb4adfbb-1.debian.tar.xz
Files:
 dc1837d748e1ecd6e5bb6e6c66a41bff 82246 bmo_0.2.3+t20240628151826.dev.gb4adfbb.orig.tar.gz
 385cf2898ef92ef8d6da55308d86d34f 1724 bmo_0.2.3+t20240628151826.dev.gb4adfbb-1.debian.tar.xz
