Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-common, vdk-pylib
Architecture: all
Version: 0.2.0-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 3d1386ebd5c7124ac9d7656b41c004833f709df6 88003 vdk_0.2.0.orig.tar.gz
 4be9391fda6b1582e042d3cc4763f51be27666e4 1144 vdk_0.2.0-1.debian.tar.xz
Checksums-Sha256:
 639187bcbce42c0c4050b66e84333f1b1ba7de27b645cff302cb6cc0af892d90 88003 vdk_0.2.0.orig.tar.gz
 bc622ada2a645bd4e7abed3991a4980aa3b250661b7ac66b22720ebd6ac96f93 1144 vdk_0.2.0-1.debian.tar.xz
Files:
 96138815768778dc8fd93bc11c48e4a8 88003 vdk_0.2.0.orig.tar.gz
 a7bfb3d9726ac40befbbdd5ca18836f3 1144 vdk_0.2.0-1.debian.tar.xz
