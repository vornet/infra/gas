Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.4+t20210305005714.pysep.g23252ad-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 020e7cef662f6b696a0366d5c46d88ec376955a3 68092 jattool_0.3.4+t20210305005714.pysep.g23252ad.orig.tar.gz
 313308558def2967ccd2741f86110791ca78f00c 1504 jattool_0.3.4+t20210305005714.pysep.g23252ad-1.debian.tar.xz
Checksums-Sha256:
 d2de8c1cf12fbf48516337d4011f01188d2d482a8597a47465dcd8b287977734 68092 jattool_0.3.4+t20210305005714.pysep.g23252ad.orig.tar.gz
 043345047d09a5ceab3babfd08a127398ecd61bf685527a5adb0b35530abfcd7 1504 jattool_0.3.4+t20210305005714.pysep.g23252ad-1.debian.tar.xz
Files:
 30790a378dc6be8f97a0245fdf4488fa 68092 jattool_0.3.4+t20210305005714.pysep.g23252ad.orig.tar.gz
 35699ab2f9d7af7d2d950c257b48a82a 1504 jattool_0.3.4+t20210305005714.pysep.g23252ad-1.debian.tar.xz
