Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.7-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 bd03f311da8bbf521acb180f2ec43d1cedd38fb4 70294 jattool_0.3.7.orig.tar.gz
 e27fe3a81d43ebf4bcd7b0877ccb019139fc04ca 1492 jattool_0.3.7-1.debian.tar.xz
Checksums-Sha256:
 3b3261bef150dc3657f4f12868b15882afbdfb475f7a634f1f17dc138a1baa9c 70294 jattool_0.3.7.orig.tar.gz
 7bc96e6d68fda6eec8c7196da1de8f722b5a06a7a5e9b038e6bc594cfd69fba8 1492 jattool_0.3.7-1.debian.tar.xz
Files:
 1d8cc9db5c416fd7cd496ec1dd3a178f 70294 jattool_0.3.7.orig.tar.gz
 eb6a33e97d4d921de639b417a42cfc13 1492 jattool_0.3.7-1.debian.tar.xz
