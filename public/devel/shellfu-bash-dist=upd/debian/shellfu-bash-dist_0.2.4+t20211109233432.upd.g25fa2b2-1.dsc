Format: 3.0 (quilt)
Source: shellfu-bash-dist
Binary: shellfu-bash-dist
Architecture: all
Version: 0.2.4+t20211109233432.upd.g25fa2b2-1
Maintainer: Alois Mahdal <netvor+dist@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-dist
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-dist deb misc extra arch=all
Checksums-Sha1:
 a4f775d9645a17381037697c9ee3dd78625c8f4f 47953 shellfu-bash-dist_0.2.4+t20211109233432.upd.g25fa2b2.orig.tar.gz
 9af5433e74d24f2f14d5fad075d6b8ae6aa3b882 880 shellfu-bash-dist_0.2.4+t20211109233432.upd.g25fa2b2-1.debian.tar.xz
Checksums-Sha256:
 71bbd5fb930c91755fbe8ec6c6a73dc1ab6e2e612f13df3ba25ff4116f5b323c 47953 shellfu-bash-dist_0.2.4+t20211109233432.upd.g25fa2b2.orig.tar.gz
 07aa0efe8ca01ce623f1922c78efaaf6bbf657202e0a28b02bb10053ec6ec59f 880 shellfu-bash-dist_0.2.4+t20211109233432.upd.g25fa2b2-1.debian.tar.xz
Files:
 ada90aa901d190139d9eeeabf4634859 47953 shellfu-bash-dist_0.2.4+t20211109233432.upd.g25fa2b2.orig.tar.gz
 2232023ec73666f4cd912cc832b6b739 880 shellfu-bash-dist_0.2.4+t20211109233432.upd.g25fa2b2-1.debian.tar.xz
