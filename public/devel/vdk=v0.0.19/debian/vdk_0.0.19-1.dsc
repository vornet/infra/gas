Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.19-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 8f7f04852d177d986823b19432c2d0ac7255a786 66078 vdk_0.0.19.orig.tar.gz
 4ca498a736a7b77c6f18428fe6aab70419a9ff2f 1012 vdk_0.0.19-1.debian.tar.xz
Checksums-Sha256:
 36d00aac670b858f19c07edb1e870631047636e2891daeaefbb1a8091acb4f4f 66078 vdk_0.0.19.orig.tar.gz
 e8621a71c8e31c5bc96c9b2e1854b4ea7e69bb5286a509858cd4a5be130ec4e6 1012 vdk_0.0.19-1.debian.tar.xz
Files:
 2999c46cdfd45e247a07bf39f907d842 66078 vdk_0.0.19.orig.tar.gz
 4d3195090de35771d1ba7676ae2a6a17 1012 vdk_0.0.19-1.debian.tar.xz
