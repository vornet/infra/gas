Format: 3.0 (quilt)
Source: imapdomo
Binary: imapdomo
Architecture: all
Version: 0.0.21-1
Maintainer: Alois Mahdal <netvor+imapdomo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/hacktop/imapdomo
Build-Depends: debhelper (>= 9), make
Package-List:
 imapdomo deb misc extra arch=all
Checksums-Sha1:
 651bae9f4123a6cf30d5948907455c2c7b6b98ff 42269 imapdomo_0.0.21.orig.tar.gz
 24ef6a68a8878b792f85accf65ddcdd12bbd02ea 932 imapdomo_0.0.21-1.debian.tar.xz
Checksums-Sha256:
 17591e050428754e0d6250523286f6c59b89d6bb6f15a8a8a35389dd5e8bf089 42269 imapdomo_0.0.21.orig.tar.gz
 43e2b9208903f1db34ed1034dc71da08aa4573f696acda9cd1169d4a422c2381 932 imapdomo_0.0.21-1.debian.tar.xz
Files:
 d4b58eb11e23f57e899a094d012b2321 42269 imapdomo_0.0.21.orig.tar.gz
 f386c20b07ffb691b9fcf171694caa26 932 imapdomo_0.0.21-1.debian.tar.xz
