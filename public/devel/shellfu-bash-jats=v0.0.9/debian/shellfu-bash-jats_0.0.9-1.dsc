Format: 3.0 (quilt)
Source: shellfu-bash-jats
Binary: shellfu-bash-jats
Architecture: all
Version: 0.0.9-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jats
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty
Package-List:
 shellfu-bash-jats deb misc extra arch=all
Checksums-Sha1:
 243f3ecb0c395f9b3758a963b7a75e2d549f1c48 49536 shellfu-bash-jats_0.0.9.orig.tar.gz
 3e8dbb1efad5ec5cb699ede14018d1a4f174a652 904 shellfu-bash-jats_0.0.9-1.debian.tar.xz
Checksums-Sha256:
 86785e892742d87c9149ce6b6f22d4900d14cdc3bbd90f3cc52573b52a0ea084 49536 shellfu-bash-jats_0.0.9.orig.tar.gz
 97c8c966285fba669395e22e9ed4ad41f87767836dd5d35983166742a41868fc 904 shellfu-bash-jats_0.0.9-1.debian.tar.xz
Files:
 12e4eb680a3bfaaa5dcea666cc177435 49536 shellfu-bash-jats_0.0.9.orig.tar.gz
 59cf9e676c683c22d409222f8a42e835 904 shellfu-bash-jats_0.0.9-1.debian.tar.xz
