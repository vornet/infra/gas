Format: 3.0 (quilt)
Source: imapdomo
Binary: imapdomo
Architecture: all
Version: 0.0.21-1
Maintainer: Alois Mahdal <netvor+imapdomo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/hacktop/imapdomo
Build-Depends: debhelper (>= 9), make
Package-List:
 imapdomo deb misc extra arch=all
Checksums-Sha1:
 d521c784918f28cc7a67bf8a382c290aab9a9821 42269 imapdomo_0.0.21.orig.tar.gz
 24ef6a68a8878b792f85accf65ddcdd12bbd02ea 932 imapdomo_0.0.21-1.debian.tar.xz
Checksums-Sha256:
 0c420529e27efb882563fe24710b17c3e127535847f02a11c38bc8d562be4356 42269 imapdomo_0.0.21.orig.tar.gz
 43e2b9208903f1db34ed1034dc71da08aa4573f696acda9cd1169d4a422c2381 932 imapdomo_0.0.21-1.debian.tar.xz
Files:
 263ee8b639e043ed2b1632c790cd33c7 42269 imapdomo_0.0.21.orig.tar.gz
 f386c20b07ffb691b9fcf171694caa26 932 imapdomo_0.0.21-1.debian.tar.xz
