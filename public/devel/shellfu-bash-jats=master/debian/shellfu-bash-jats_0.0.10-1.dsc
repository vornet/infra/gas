Format: 3.0 (quilt)
Source: shellfu-bash-jats
Binary: shellfu-bash-jats
Architecture: all
Version: 0.0.10-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jats
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.44), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty
Package-List:
 shellfu-bash-jats deb misc extra arch=all
Checksums-Sha1:
 e0d8edde3613c1b898d05dd9e94db5bd10bf9755 49520 shellfu-bash-jats_0.0.10.orig.tar.gz
 b63b7d2cddcbc8123aba7beec1805414729eb5f3 908 shellfu-bash-jats_0.0.10-1.debian.tar.xz
Checksums-Sha256:
 2b4654b8ce6db60e8312364d04e09848c683a74f7d448a5b44cb699c7fdd4561 49520 shellfu-bash-jats_0.0.10.orig.tar.gz
 529fab211fcbfeb8d78c2b255b1e024044db4a2d9cf4fb7258f67c83d571264e 908 shellfu-bash-jats_0.0.10-1.debian.tar.xz
Files:
 d17de46cabc54cf89099953e1440f027 49520 shellfu-bash-jats_0.0.10.orig.tar.gz
 ebcc10f32604a6a9282400d1e552cb8b 908 shellfu-bash-jats_0.0.10-1.debian.tar.xz
