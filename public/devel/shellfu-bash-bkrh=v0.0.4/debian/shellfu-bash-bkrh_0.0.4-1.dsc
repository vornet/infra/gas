Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.4-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 4d597a8c523093e661452113b0e89f7e305cd78d 35296 shellfu-bash-bkrh_0.0.4.orig.tar.gz
 550eef728b0f855425ba3a56e43215899f554123 820 shellfu-bash-bkrh_0.0.4-1.debian.tar.xz
Checksums-Sha256:
 0be3f320294700b5394cd822d12fb8190607698353e4bd44aaaa63fee195c136 35296 shellfu-bash-bkrh_0.0.4.orig.tar.gz
 7cb215808a3767861bce116905322d0aa3388776a6aa7a2f8b859125cddde036 820 shellfu-bash-bkrh_0.0.4-1.debian.tar.xz
Files:
 4bb7fec74fecc8422dc4f782563f83a0 35296 shellfu-bash-bkrh_0.0.4.orig.tar.gz
 1e23213b796e73d8b3bb515e600ce82b 820 shellfu-bash-bkrh_0.0.4-1.debian.tar.xz
