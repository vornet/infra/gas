Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-common, vdk-pylib
Architecture: all
Version: 0.2.1-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 3a3d59ab5b760e9639c2c892f88f4bf566dd0d1d 88003 vdk_0.2.1.orig.tar.gz
 57daf855e34cf965b0b61219ca77565b6c875886 1144 vdk_0.2.1-1.debian.tar.xz
Checksums-Sha256:
 2c90ea263d3b09851d591e0fda2dd43e013f21555db58be3735ad8bf80d6d621 88003 vdk_0.2.1.orig.tar.gz
 97bd695691d7e8b95e4c8cae02815abd8d4d1ae0591d5d812b9bb58ebb5b7199 1144 vdk_0.2.1-1.debian.tar.xz
Files:
 721b74ff2e94523fc8865403fe9decfa 88003 vdk_0.2.1.orig.tar.gz
 d2bd0fd4de0e622b3b6814b130b269b3 1144 vdk_0.2.1-1.debian.tar.xz
