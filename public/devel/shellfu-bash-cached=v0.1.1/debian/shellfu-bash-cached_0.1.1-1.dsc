Format: 3.0 (quilt)
Source: shellfu-bash-cached
Binary: shellfu-bash-cached
Architecture: all
Version: 0.1.1-1
Maintainer: Alois Mahdal <netvor+cached@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-cached
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-cached deb misc extra arch=all
Checksums-Sha1:
 03e4a08903d8a81a53fa28a9cc680365354f4895 34876 shellfu-bash-cached_0.1.1.orig.tar.gz
 0aa27333103d32a9f4bdd35684314c6a4e63ad00 876 shellfu-bash-cached_0.1.1-1.debian.tar.xz
Checksums-Sha256:
 4aa38a3d8bf8cdc0f5f58fb978ce0198b09d87336e4cdd8cf4b4b912e7b6e09a 34876 shellfu-bash-cached_0.1.1.orig.tar.gz
 ebc81002202e62273f1526f3a5a874bb20ad0a74879d638b45b072bda859171a 876 shellfu-bash-cached_0.1.1-1.debian.tar.xz
Files:
 c62d0a9000bc7849628e09e2a9109604 34876 shellfu-bash-cached_0.1.1.orig.tar.gz
 0c9f77f09fa4ea45e186dac7e0c25e1b 876 shellfu-bash-cached_0.1.1-1.debian.tar.xz
