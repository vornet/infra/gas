Format: 3.0 (quilt)
Source: jats-git
Binary: jats-git
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <amahdal@redhat.com>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-git deb misc extra arch=all
Checksums-Sha1:
 a705db36451b30938c9f4a10d3f443cded0d5dca 2138 jats-git_0.0.5.orig.tar.gz
 e81ca6f64b51599e91079f398fdc6623755c2066 804 jats-git_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 b461cfe812db4811b1ec005a2e3db3ba4753c62c2edbf9a99631b686016a21fb 2138 jats-git_0.0.5.orig.tar.gz
 f966a9c3427da850b25b7a4eaeb0f21127ba251c17aff6dc016f6ec47c240b3d 804 jats-git_0.0.5-1.debian.tar.xz
Files:
 6695094e55e4ae0ad9ceb4618c1e7d79 2138 jats-git_0.0.5.orig.tar.gz
 a88d57299d84b8862035aea829aedf19 804 jats-git_0.0.5-1.debian.tar.xz
