Format: 3.0 (quilt)
Source: vdk
Binary: vdk-common, vdk-pylib, vdk-zigbin
Architecture: all
Version: 0.2.7-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
 vdk-zigbin deb misc extra arch=all
Checksums-Sha1:
 3e78976ee0ac2de9626c70c9f7a7917614de811b 92180 vdk_0.2.7.orig.tar.gz
 3d6ee00e6745fa7777475b08f92836779aa42c02 1240 vdk_0.2.7-1.debian.tar.xz
Checksums-Sha256:
 28ecb11024752a57b72d00aba9f991867bd57c2dafd855e2f239ec6e0419396e 92180 vdk_0.2.7.orig.tar.gz
 300a619a5ca07dd1e482bde233a57bb1134a3ec72297d7c9c3e0dce327636a3a 1240 vdk_0.2.7-1.debian.tar.xz
Files:
 9e734683a28405c49d1f8196492bdbf1 92180 vdk_0.2.7.orig.tar.gz
 a99a5f2c2448384ae9ad0e5f7f74e9ea 1240 vdk_0.2.7-1.debian.tar.xz
