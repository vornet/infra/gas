Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.50-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 4edd7f242cfe0bb95ccb5c90cc386240a62506af 93123 shellfu_0.10.50.orig.tar.gz
 13b950b7133625a6d0bb4de5df423339be90da49 2228 shellfu_0.10.50-1.debian.tar.xz
Checksums-Sha256:
 4c223bd78c6f72e4cf77c6dd0644b92e3aa81cd087918c107d14a5d88d31c0d8 93123 shellfu_0.10.50.orig.tar.gz
 adb7515487c19713668d05d8044971b3b9755bbeae056f2235a4fc26596c6ad6 2228 shellfu_0.10.50-1.debian.tar.xz
Files:
 71396deccc09346bdcd6b7f6cdde81fd 93123 shellfu_0.10.50.orig.tar.gz
 73c7102bea20009fd06017d7dbb0080a 2228 shellfu_0.10.50-1.debian.tar.xz
