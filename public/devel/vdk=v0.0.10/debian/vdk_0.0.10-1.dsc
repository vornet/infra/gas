Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.10-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 9f86e371e66a0604d6ed6e2b0ca3f29828d0e81a 61364 vdk_0.0.10.orig.tar.gz
 fd811c4c2bb1c932a657f71fe8f3e3895c81e08d 948 vdk_0.0.10-1.debian.tar.xz
Checksums-Sha256:
 5feb2589286e8997e32906f792ccc86dac80115ec9ec3cbc22ee0520091b1d5f 61364 vdk_0.0.10.orig.tar.gz
 8366828582c4ba405838f7e5f5e0d0f8d55bb34a4633457f932f7e5fb19bec14 948 vdk_0.0.10-1.debian.tar.xz
Files:
 9a597f67cbd7aec8a2faef26e4a143d3 61364 vdk_0.0.10.orig.tar.gz
 689b8949b5d3784045010e8b00ad1f4c 948 vdk_0.0.10-1.debian.tar.xz
