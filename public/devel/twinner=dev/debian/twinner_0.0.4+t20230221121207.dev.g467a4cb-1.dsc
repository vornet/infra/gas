Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.4+t20230221121207.dev.g467a4cb-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 a80792e6280539e8accf7528d7e1c8496136b34d 33721 twinner_0.0.4+t20230221121207.dev.g467a4cb.orig.tar.gz
 324a8d1d48f02c96013f0869c6af5dfd56295db6 820 twinner_0.0.4+t20230221121207.dev.g467a4cb-1.debian.tar.xz
Checksums-Sha256:
 b6a3356bfe18fd7f4398b6fb9f89a533e2216d745178e09aa144d68187fb2bc8 33721 twinner_0.0.4+t20230221121207.dev.g467a4cb.orig.tar.gz
 07726d52b50ab5332df1ebaf5035c8ba2791ba4e0d13b10ed3101521d5891b54 820 twinner_0.0.4+t20230221121207.dev.g467a4cb-1.debian.tar.xz
Files:
 68d137220cb68b2b9af65282255b6104 33721 twinner_0.0.4+t20230221121207.dev.g467a4cb.orig.tar.gz
 1989218d14faf7db7cb05c8af498e230 820 twinner_0.0.4+t20230221121207.dev.g467a4cb-1.debian.tar.xz
