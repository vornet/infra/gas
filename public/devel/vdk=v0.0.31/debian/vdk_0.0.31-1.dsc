Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.31-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 dab95d9d76d4eca6d24a6020f924ed439ff73f22 75398 vdk_0.0.31.orig.tar.gz
 c4691c4fbd3d0090a16de9ded517e3239d22a5de 1072 vdk_0.0.31-1.debian.tar.xz
Checksums-Sha256:
 236e6ebdcf527bfd46cb5923478a3294bec79d6ea64b9f2777f155b4bbcfb58a 75398 vdk_0.0.31.orig.tar.gz
 44cae11b55c79b08d938148e43751e54edf41f90a18f52ff8e6833d65d2a3e55 1072 vdk_0.0.31-1.debian.tar.xz
Files:
 9403681489023936b44c2e36a1da0711 75398 vdk_0.0.31.orig.tar.gz
 0c84464668d7d098ccb50ae8f75ed3eb 1072 vdk_0.0.31-1.debian.tar.xz
