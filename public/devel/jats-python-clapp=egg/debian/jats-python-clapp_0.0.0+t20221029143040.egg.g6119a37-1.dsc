Format: 3.0 (quilt)
Source: jats-python-clapp
Binary: jats-python-clapp
Architecture: all
Version: 0.0.0+t20221029143040.egg.g6119a37-1
Maintainer: Alois Mahdal <netvor+clapp@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-python-clapp deb misc extra arch=all
Checksums-Sha1:
 af05679d858477ea9447f62a5cce5b1dc54de14f 1807 jats-python-clapp_0.0.0+t20221029143040.egg.g6119a37.orig.tar.gz
 896b7cbf156d5ed226e60869f08cdfa0c15b9f77 824 jats-python-clapp_0.0.0+t20221029143040.egg.g6119a37-1.debian.tar.xz
Checksums-Sha256:
 efb0ae6b9500fdf2ec7b159c99bf5d8010f2ed6e25a06b5d32446f0319f2afda 1807 jats-python-clapp_0.0.0+t20221029143040.egg.g6119a37.orig.tar.gz
 bd3175bbfce173f1a28296a4ad96d6a06fde9823e8b8f992de42633a452620bf 824 jats-python-clapp_0.0.0+t20221029143040.egg.g6119a37-1.debian.tar.xz
Files:
 b34b1106e6782b033604f1f60cf4b3a1 1807 jats-python-clapp_0.0.0+t20221029143040.egg.g6119a37.orig.tar.gz
 e8c632e95fa84177f0680b1e1c53ca79 824 jats-python-clapp_0.0.0+t20221029143040.egg.g6119a37-1.debian.tar.xz
