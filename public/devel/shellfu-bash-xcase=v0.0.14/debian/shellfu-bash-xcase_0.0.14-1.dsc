Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.14-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 81ddc86b7ba9ccd7ec38d390541cd0a9521f56a6 46565 shellfu-bash-xcase_0.0.14.orig.tar.gz
 72d0391a2132cdeccee75c3ff66ff351a6c810a9 1016 shellfu-bash-xcase_0.0.14-1.debian.tar.xz
Checksums-Sha256:
 d97d6d4b9ad53187cbe7eca016f25648e3727ebbc553db80d219972d2a3aafbe 46565 shellfu-bash-xcase_0.0.14.orig.tar.gz
 f6d7592f147a3b44597ee68d139486137a11cb7473d602cccf3808c6a8103f1d 1016 shellfu-bash-xcase_0.0.14-1.debian.tar.xz
Files:
 d7f670d2fd024d25a00f4265acfafc43 46565 shellfu-bash-xcase_0.0.14.orig.tar.gz
 03310c4a2ef9bd7b296e9593f0d285e4 1016 shellfu-bash-xcase_0.0.14-1.debian.tar.xz
