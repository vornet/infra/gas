Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.35-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 ebcded32eb8b165ee42636fdcc7d5087b652a4ba 75965 vdk_0.0.35.orig.tar.gz
 54aa599f63760d3953bc4e2900031d8940b03f23 1080 vdk_0.0.35-1.debian.tar.xz
Checksums-Sha256:
 772b6b9081179cd364b350da03d9acb9e0f6ce064dd5414682822025b831f2a7 75965 vdk_0.0.35.orig.tar.gz
 058fc0f3a0fdf62edaffe96bec9167f19c5d47d89a7a1147c1d63a198dc96eb7 1080 vdk_0.0.35-1.debian.tar.xz
Files:
 92eb7d7a6061888af3f1f0d2e2a4bb5d 75965 vdk_0.0.35.orig.tar.gz
 943df5f0c8257fd96fa561b0304fd711 1080 vdk_0.0.35-1.debian.tar.xz
