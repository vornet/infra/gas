Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.13-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 e6279f67b074f9bac0e4e16cb2e97869c1244f7c 45238 shellfu-bash-xcase_0.0.13.orig.tar.gz
 05fb0e752a6528284c274d67183f7efce243e82d 1016 shellfu-bash-xcase_0.0.13-1.debian.tar.xz
Checksums-Sha256:
 2dfc65f89992de83f378783aa7636b72072fd214cb36fcfc5969b6ffeb14a74d 45238 shellfu-bash-xcase_0.0.13.orig.tar.gz
 eec88590a0c2d1d9ed9c784b8145d04de34d5f399fb638a3af7638e390c1195f 1016 shellfu-bash-xcase_0.0.13-1.debian.tar.xz
Files:
 1c5a09fd6192995f24ffd26325d4be46 45238 shellfu-bash-xcase_0.0.13.orig.tar.gz
 08269e1cfc611fd84560691b862d042a 1016 shellfu-bash-xcase_0.0.13-1.debian.tar.xz
