Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.51+t20220118174931.desfd.g0cff43d-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 fd2658651b54bfd036c7e5d8aa078f35f0e4e32d 94054 shellfu_0.10.51+t20220118174931.desfd.g0cff43d.orig.tar.gz
 b8863983823db488054f7c174edb0917cfdc5c6d 2248 shellfu_0.10.51+t20220118174931.desfd.g0cff43d-1.debian.tar.xz
Checksums-Sha256:
 2b6ac4d4d434085671b23137ff593a78a90a31afc010348adc3086711b61185f 94054 shellfu_0.10.51+t20220118174931.desfd.g0cff43d.orig.tar.gz
 c28c2709491a18e0165ab4cc468987edaa1b4c8bb62e4194620c3068f80524d3 2248 shellfu_0.10.51+t20220118174931.desfd.g0cff43d-1.debian.tar.xz
Files:
 dc04a2127603478102ba668e81c9a760 94054 shellfu_0.10.51+t20220118174931.desfd.g0cff43d.orig.tar.gz
 710de58bbda636edb226c0ddf7ec907a 2248 shellfu_0.10.51+t20220118174931.desfd.g0cff43d-1.debian.tar.xz
