Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.8-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 b90ecc579acd377601a8ff0f6c1de54b1ea05c3f 84412 shellfu-bash-jat_0.2.8.orig.tar.gz
 b8125f48e76ff47111f45654d81250fb24da17e2 1004 shellfu-bash-jat_0.2.8-1.debian.tar.xz
Checksums-Sha256:
 023a64401ba0c0c7bfc03bc18907a3754bf7c5b69719a9e6fc2150960f93a7e8 84412 shellfu-bash-jat_0.2.8.orig.tar.gz
 ff77523607107e772142196eb1388923f56ba74b9b0da8e31a901336f2e3aaa8 1004 shellfu-bash-jat_0.2.8-1.debian.tar.xz
Files:
 d659f85aad142586680ee8f5cce84645 84412 shellfu-bash-jat_0.2.8.orig.tar.gz
 c20db7f78aba0f70c13ee4de00134636 1004 shellfu-bash-jat_0.2.8-1.debian.tar.xz
