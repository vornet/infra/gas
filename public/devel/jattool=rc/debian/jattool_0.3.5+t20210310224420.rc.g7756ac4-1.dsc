Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.5+t20210310224420.rc.g7756ac4-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 5571fdc75a537163e4d1f7390d4ba22f65791d41 68090 jattool_0.3.5+t20210310224420.rc.g7756ac4.orig.tar.gz
 c400a0fc2f6e33b15321db1f9a49ef547008c9cf 1508 jattool_0.3.5+t20210310224420.rc.g7756ac4-1.debian.tar.xz
Checksums-Sha256:
 00f662b111036d9abf24f999dfc1c21b0664a5f15fe37196e5c36dbe3c06acc5 68090 jattool_0.3.5+t20210310224420.rc.g7756ac4.orig.tar.gz
 2d2fef24758761ea7764350774c5fae6703ba16b9874f25716c833368e000e77 1508 jattool_0.3.5+t20210310224420.rc.g7756ac4-1.debian.tar.xz
Files:
 e1eee1c2a53ce62ad7da499bd4179807 68090 jattool_0.3.5+t20210310224420.rc.g7756ac4.orig.tar.gz
 35ab1cf854a052097bcf49632e59f14f 1508 jattool_0.3.5+t20210310224420.rc.g7756ac4-1.debian.tar.xz
