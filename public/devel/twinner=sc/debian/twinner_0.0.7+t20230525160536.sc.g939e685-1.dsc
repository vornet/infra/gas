Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.7+t20230525160536.sc.g939e685-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 9c6fff303abd36f360cb0e24f10fae082ff17b99 34109 twinner_0.0.7+t20230525160536.sc.g939e685.orig.tar.gz
 4bf59dea842cba5088f4f4612874fa85cb404c85 824 twinner_0.0.7+t20230525160536.sc.g939e685-1.debian.tar.xz
Checksums-Sha256:
 1aa2e02feb646388f716018426be2bd288fa167b828183c5292151d74d36a709 34109 twinner_0.0.7+t20230525160536.sc.g939e685.orig.tar.gz
 3cd06ab11099e9a04d4a479d0eb677e9b71335426dfe4f23a4c9bec911a877b9 824 twinner_0.0.7+t20230525160536.sc.g939e685-1.debian.tar.xz
Files:
 55525c3e0e617bd9bed96a4ec8bb6da8 34109 twinner_0.0.7+t20230525160536.sc.g939e685.orig.tar.gz
 11564e4ecb89e051301e0101274b4a11 824 twinner_0.0.7+t20230525160536.sc.g939e685-1.debian.tar.xz
