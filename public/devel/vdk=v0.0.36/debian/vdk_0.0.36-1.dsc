Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.36-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 d109a4510d2fdd6f404cd6122527288e39d3bab7 75948 vdk_0.0.36.orig.tar.gz
 c365a9bb97c94efa79b97148d3172b937218f1c7 1084 vdk_0.0.36-1.debian.tar.xz
Checksums-Sha256:
 41553855b768f2301d6f2903202dbc57d806fa84d6d78aea9f00c8c8ffa1e0b6 75948 vdk_0.0.36.orig.tar.gz
 db3156d31d48cf6aa7cdfc03e14016c5e9e53e2a830c54366befcee8e7eacbf5 1084 vdk_0.0.36-1.debian.tar.xz
Files:
 92375bdaad52940cf5d50f5e2d758155 75948 vdk_0.0.36.orig.tar.gz
 a1261936aba816e0d4eb1bedeb47ac52 1084 vdk_0.0.36-1.debian.tar.xz
