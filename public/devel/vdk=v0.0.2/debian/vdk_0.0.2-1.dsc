Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper (>= 9), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 af13a5896b348494d16dbb8d1597f16cecbb9d97 60555 vdk_0.0.2.orig.tar.gz
 8024474417fd8d58b9cb2df6b011b79b14967544 992 vdk_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 0981e8d5004513296a50a6ae1d07d59263bf147e77b19c3055041307da022da0 60555 vdk_0.0.2.orig.tar.gz
 2e8cf17c5bd0542611a4bfbdb389ddca4738b2f733a30215ad2260c5165bb639 992 vdk_0.0.2-1.debian.tar.xz
Files:
 fef2fe843e0ac8dfbf8f62d9bd599f6d 60555 vdk_0.0.2.orig.tar.gz
 f913d2374cd5b86bb7528aa736e20588 992 vdk_0.0.2-1.debian.tar.xz
