Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.17-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), shellfu (>= 0.10.11), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0), xclip
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 fcc5a44e27dc057b2585ce9df1cf248a1058b2ff 68933 bmo_0.1.17.orig.tar.gz
 b245f8b1bde07e4f53b42a6652ba71200d2b6e5f 1600 bmo_0.1.17-1.debian.tar.xz
Checksums-Sha256:
 4025309887479e8296e0e31504045da40c08cfbe3df8285b0531c48091b2436e 68933 bmo_0.1.17.orig.tar.gz
 f9534b3711b4fd8b72fbf82664e90b5a00959f3869e6be904f1ef73678d9f60d 1600 bmo_0.1.17-1.debian.tar.xz
Files:
 481508cd04577d4ab836874649da96cf 68933 bmo_0.1.17.orig.tar.gz
 30af113b3b5a9d02031963b926c969f4 1600 bmo_0.1.17-1.debian.tar.xz
