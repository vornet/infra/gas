Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.48+t20220104225012.sfep.g319b754-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 752bb2d2479b978266b23627d3f4545e299e959f 93856 shellfu_0.10.48+t20220104225012.sfep.g319b754.orig.tar.gz
 c73057b114ac3772f3dedb6458d2b936e1a239f3 2244 shellfu_0.10.48+t20220104225012.sfep.g319b754-1.debian.tar.xz
Checksums-Sha256:
 31f734078e623ae0bb3bcad70173b83fa2e2df79b36bbd57a25b4ecc97c91424 93856 shellfu_0.10.48+t20220104225012.sfep.g319b754.orig.tar.gz
 508e0db8457a562a165e66d01692b41ce7ed14adf2a81e4624bf4a51a14c92eb 2244 shellfu_0.10.48+t20220104225012.sfep.g319b754-1.debian.tar.xz
Files:
 0fc3042067d86b0c70990bf661d5f0c1 93856 shellfu_0.10.48+t20220104225012.sfep.g319b754.orig.tar.gz
 ce02fc3e82e832fbcdbe9a2a92077b0f 2244 shellfu_0.10.48+t20220104225012.sfep.g319b754-1.debian.tar.xz
