Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.32-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 1fe0bb88c4e47aa636f1ad8acc73a9d82c4e2343 75859 vdk_0.0.32.orig.tar.gz
 c272713d67a8823e81bcc405b50366c3b53051ad 1072 vdk_0.0.32-1.debian.tar.xz
Checksums-Sha256:
 1563c4cbe681f85ad28c4f9081b141653529da5876564680c8359eb85cc2ea66 75859 vdk_0.0.32.orig.tar.gz
 34b045bbf486a5bda5a321405eac18406087626f322ff832096ac39a633dfd29 1072 vdk_0.0.32-1.debian.tar.xz
Files:
 80b78484e5bb6b3815a5172f97b8a565 75859 vdk_0.0.32.orig.tar.gz
 c3e12bdbfe83acbe5ba229a774f5baf2 1072 vdk_0.0.32-1.debian.tar.xz
