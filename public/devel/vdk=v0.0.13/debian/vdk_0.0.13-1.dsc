Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.13-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 461ce23131c3e471a9b6768e75bc2cbcda3fe5d5 62465 vdk_0.0.13.orig.tar.gz
 7ba0c6b84b8015f9f3936907b04b2d2cb78afa34 952 vdk_0.0.13-1.debian.tar.xz
Checksums-Sha256:
 f45a827c0424059ea1a8b4f688a03e964a121f7a263cad95652a0bd5dd7314cd 62465 vdk_0.0.13.orig.tar.gz
 df5dc63196cb3198e46555e8c24b2dedd6b1b34e6510392888ad6b11d3d44ac7 952 vdk_0.0.13-1.debian.tar.xz
Files:
 7847aeb615c1e38fc7bd07d6888c1497 62465 vdk_0.0.13.orig.tar.gz
 e42f393be59850acf13a0fe00e52e122 952 vdk_0.0.13-1.debian.tar.xz
