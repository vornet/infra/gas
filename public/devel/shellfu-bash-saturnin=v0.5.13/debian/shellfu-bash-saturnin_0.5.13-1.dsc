Format: 3.0 (quilt)
Source: shellfu-bash-saturnin
Binary: shellfu-bash-saturnin
Architecture: all
Version: 0.5.13-1
Maintainer: Alois Mahdal <netvor+saturnin@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-saturnin
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-inigrep, shellfu-bash-pretty, shellfu-sh-exit
Package-List:
 shellfu-bash-saturnin deb misc extra arch=all
Checksums-Sha1:
 74654a8d12a70d9b096725ec3128d987d00f10d9 57849 shellfu-bash-saturnin_0.5.13.orig.tar.gz
 4fd0511815280b0dc3124c56e7a3c84444dd3b3f 1096 shellfu-bash-saturnin_0.5.13-1.debian.tar.xz
Checksums-Sha256:
 e8e9d19305661f95c50aee7da9ce4d16aa64e586d4c7b9e8675f0b00398b9d71 57849 shellfu-bash-saturnin_0.5.13.orig.tar.gz
 bef5e11a3e0b0bc515f1a4d8ba26e283c3e61e872d5d9d020e96875929d33634 1096 shellfu-bash-saturnin_0.5.13-1.debian.tar.xz
Files:
 d1cda0f36243bee59d0a1c1490263619 57849 shellfu-bash-saturnin_0.5.13.orig.tar.gz
 f03c4e50fe621b4abd0243d962644266 1096 shellfu-bash-saturnin_0.5.13-1.debian.tar.xz
