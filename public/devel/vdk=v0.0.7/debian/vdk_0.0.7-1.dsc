Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.7-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 a4acdab02e5f4e87f83aec4cb9b30ee9851a1cab 61087 vdk_0.0.7.orig.tar.gz
 a51c6f91480417068547fbd94867f1ea9fc39f38 948 vdk_0.0.7-1.debian.tar.xz
Checksums-Sha256:
 c4e23a1c2bbe75c673dc1d299e9261450902811132af9f85a078fa66e56d1d66 61087 vdk_0.0.7.orig.tar.gz
 71abe73733483000b7e0357753192382fb3508f36365ee7dd3ee467682beb962 948 vdk_0.0.7-1.debian.tar.xz
Files:
 03d7cd140169272b8e8a9bb7a05d47f2 61087 vdk_0.0.7.orig.tar.gz
 21d52e63a8301f8a9d91e2765cdbe27e 948 vdk_0.0.7-1.debian.tar.xz
