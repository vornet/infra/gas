Format: 3.0 (quilt)
Source: shellfu-bash-inigrep
Binary: shellfu-bash-inigrep
Architecture: all
Version: 0.11.3-1
Maintainer: Alois Mahdal <netvor+inigrep@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-inigrep
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-inigrep deb misc extra arch=all
Checksums-Sha1:
 ecccd4d608381f71220a41215ee4ac8f0ca30343 76123 shellfu-bash-inigrep_0.11.3.orig.tar.gz
 bb03202bc5f9aca67fe4e830fea5279d449ce62d 924 shellfu-bash-inigrep_0.11.3-1.debian.tar.xz
Checksums-Sha256:
 23f373173cf6363965ff34d2435db4d621eb6fe18d0814a3a64f0b9b91d8103f 76123 shellfu-bash-inigrep_0.11.3.orig.tar.gz
 78887dcc5d8ccead88085b3c4a0bbaaf356366d054e3ea0870b7413d139dbbce 924 shellfu-bash-inigrep_0.11.3-1.debian.tar.xz
Files:
 c49bd355a5995a773dc9acb5a94a883b 76123 shellfu-bash-inigrep_0.11.3.orig.tar.gz
 bb3ca2d5c8148b0303730dbdc2d9be61 924 shellfu-bash-inigrep_0.11.3-1.debian.tar.xz
