Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.25-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 72a0b2b8757f54e87cf75cf10d89acd4695113ca 73646 vdk_0.0.25.orig.tar.gz
 b845341e9d409fbcb74b1df31fc4213ca114e74c 1028 vdk_0.0.25-1.debian.tar.xz
Checksums-Sha256:
 33eb13833adfa990104d8fca19120d2a55d75fb981e5f78e2bcb7e8bfb2c96ea 73646 vdk_0.0.25.orig.tar.gz
 742d575fc107d98611e6d6107b975f434a078abb52c2939d39333a178275c536 1028 vdk_0.0.25-1.debian.tar.xz
Files:
 760de85def561a55fdce616d3a7f817d 73646 vdk_0.0.25.orig.tar.gz
 4354ef4f2f0f02c984e55d1d00a00534 1028 vdk_0.0.25-1.debian.tar.xz
