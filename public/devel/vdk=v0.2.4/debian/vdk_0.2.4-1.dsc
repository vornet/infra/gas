Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-common, vdk-pylib
Architecture: all
Version: 0.2.4-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 5f670315133ad885ae440ecd9a1b730346f25087 88272 vdk_0.2.4.orig.tar.gz
 d2cf7c9c184fde968a028c0624dbcb19c623868d 1144 vdk_0.2.4-1.debian.tar.xz
Checksums-Sha256:
 7bc4cf0f02720aed5b6e6614e9d315bf2e4298099d9b6f3917f0f1d6ccf36000 88272 vdk_0.2.4.orig.tar.gz
 53e92f85b0d5ef2aafa58a8a4a934d7d9a20273c60dc7b5a37438c05b5c143e2 1144 vdk_0.2.4-1.debian.tar.xz
Files:
 664230d3c4027d48da2f25ece6b67aff 88272 vdk_0.2.4.orig.tar.gz
 7987724b3d4af58d6fa6ccca9143380f 1144 vdk_0.2.4-1.debian.tar.xz
