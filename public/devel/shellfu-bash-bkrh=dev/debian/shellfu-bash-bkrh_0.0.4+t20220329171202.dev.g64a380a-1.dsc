Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.4+t20220329171202.dev.g64a380a-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 222a0d76d23e9d64f7bab55b93d55f6fed8aceb9 38322 shellfu-bash-bkrh_0.0.4+t20220329171202.dev.g64a380a.orig.tar.gz
 3cc559c14c82ba36156897de1d7b63d61498df71 844 shellfu-bash-bkrh_0.0.4+t20220329171202.dev.g64a380a-1.debian.tar.xz
Checksums-Sha256:
 0eb94b8c809c4369bc60384f611b7770c876f6a7f646c8f42db195113624f544 38322 shellfu-bash-bkrh_0.0.4+t20220329171202.dev.g64a380a.orig.tar.gz
 b26a0d926a6d5868dcb19566b245847c0f2a7c3113bd46f3355bd0ee2fa6a477 844 shellfu-bash-bkrh_0.0.4+t20220329171202.dev.g64a380a-1.debian.tar.xz
Files:
 cab0ea938db78bdddfd117fd1020b7cf 38322 shellfu-bash-bkrh_0.0.4+t20220329171202.dev.g64a380a.orig.tar.gz
 d731d34b95e1cdca1ea34f2b566fab5b 844 shellfu-bash-bkrh_0.0.4+t20220329171202.dev.g64a380a-1.debian.tar.xz
