Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.28-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 d973467bcd5a2f7356496a6e0a66db59e0da861f 75368 vdk_0.0.28.orig.tar.gz
 97e8885a810db2143b44137859bd7156d1f6a083 1060 vdk_0.0.28-1.debian.tar.xz
Checksums-Sha256:
 7b659d6512bb2e7191b74a8c79c50bd7c9dc67142319c1bd6798c945f0a026f7 75368 vdk_0.0.28.orig.tar.gz
 1eb7da80e1bd68ac14d90a4214cc5bde3d111beccb0918384c010d66cfa7b568 1060 vdk_0.0.28-1.debian.tar.xz
Files:
 71166c4a00782e9d14660f73c57e329c 75368 vdk_0.0.28.orig.tar.gz
 d81b4ac548a7531e8e0c8b66852563e0 1060 vdk_0.0.28-1.debian.tar.xz
