Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.1-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper (>= 9), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 d43bc8af80b1e8a88743058435a08af3d8a7731f 60553 vdk_0.0.1.orig.tar.gz
 cda1b90a26cf68c2d6b4acd9c05224ef7e9b53eb 992 vdk_0.0.1-1.debian.tar.xz
Checksums-Sha256:
 496b740cea7e369507cd4a6a0a1afc95a5ee1495bb63f46000ce14dbb26107c1 60553 vdk_0.0.1.orig.tar.gz
 7c48e02fd4db1998d87de41568a0813cd38ee100fcf7093e5f80a3a73ab5b640 992 vdk_0.0.1-1.debian.tar.xz
Files:
 bdfd4b9f5354dbc7483f1ff4bcb48f37 60553 vdk_0.0.1.orig.tar.gz
 b9901f66d3d6335473001b6d7b61cdb8 992 vdk_0.0.1-1.debian.tar.xz
