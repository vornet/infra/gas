Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.8-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 4360989bed8cf3740d5a1771640cd8999467f60c 34051 twinner_0.0.8.orig.tar.gz
 80cda58dcf41a81973cefbf4070138395c9e5ec0 804 twinner_0.0.8-1.debian.tar.xz
Checksums-Sha256:
 07cefcac420346ba5f768b030e68cad076e957f6f1ef99f7d72c9aee967bbeac 34051 twinner_0.0.8.orig.tar.gz
 5e23c817c09acd0e834e4fcd6f1970ac89fd1bdecca4cabb9263052914d59c56 804 twinner_0.0.8-1.debian.tar.xz
Files:
 ca60e292bfb595db7e04b95d0fe58281 34051 twinner_0.0.8.orig.tar.gz
 237ead27b4921b6677cc694530cc684b 804 twinner_0.0.8-1.debian.tar.xz
