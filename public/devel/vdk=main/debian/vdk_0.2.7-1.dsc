Format: 3.0 (quilt)
Source: vdk
Binary: vdk-common, vdk-pylib, vdk-zigbin
Architecture: all
Version: 0.2.7-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
 vdk-zigbin deb misc extra arch=all
Checksums-Sha1:
 6b1d2251f66a31efa73409e32aa0759089cd76ea 92198 vdk_0.2.7.orig.tar.gz
 3d6ee00e6745fa7777475b08f92836779aa42c02 1240 vdk_0.2.7-1.debian.tar.xz
Checksums-Sha256:
 87d09fe17797933b55d29bea44d1246a6c5afe0f92f292732c958f3c871c0601 92198 vdk_0.2.7.orig.tar.gz
 300a619a5ca07dd1e482bde233a57bb1134a3ec72297d7c9c3e0dce327636a3a 1240 vdk_0.2.7-1.debian.tar.xz
Files:
 ed8e910dfe581b759e9d35feec5e8334 92198 vdk_0.2.7.orig.tar.gz
 a99a5f2c2448384ae9ad0e5f7f74e9ea 1240 vdk_0.2.7-1.debian.tar.xz
