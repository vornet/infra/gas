Format: 3.0 (quilt)
Source: shellfu-sh-charmenu
Binary: shellfu-sh-charmenu
Architecture: all
Version: 0.0.0-1
Maintainer: Alois Mahdal <netvor+charmenu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-sh-charmenu
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-sh-charmenu deb misc extra arch=all
Checksums-Sha1:
 b5874c093c691bb4b45d98528f8b52a7d0dc91b7 33319 shellfu-sh-charmenu_0.0.0.orig.tar.gz
 3ddb07f93b74165bf0f46b2eb712cdf79cd281a6 924 shellfu-sh-charmenu_0.0.0-1.debian.tar.xz
Checksums-Sha256:
 282658f557b81d018c78eaabd228b00db848b2c44d23525738ac5103424e839a 33319 shellfu-sh-charmenu_0.0.0.orig.tar.gz
 6938ede849c52b19d85c99d080889fb50d55c2fa87f2afb0ea2dc5e61a48c9ba 924 shellfu-sh-charmenu_0.0.0-1.debian.tar.xz
Files:
 39ace59d8fb25bff4eeb55442f55da22 33319 shellfu-sh-charmenu_0.0.0.orig.tar.gz
 5d1a40c80ab38f38c8371b77b9d86386 924 shellfu-sh-charmenu_0.0.0-1.debian.tar.xz
