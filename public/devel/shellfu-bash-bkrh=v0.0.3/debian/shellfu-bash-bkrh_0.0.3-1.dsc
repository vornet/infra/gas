Format: 3.0 (quilt)
Source: shellfu-bash-bkrh
Binary: shellfu-bash-bkrh
Architecture: all
Version: 0.0.3-1
Maintainer: Alois Mahdal <netvor+bkrh@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-bkrh
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-bkrh deb misc extra arch=all
Checksums-Sha1:
 7cc40760f97eadf24a59685dd0142167ee60875e 34026 shellfu-bash-bkrh_0.0.3.orig.tar.gz
 0bdcdb975c70e4e251c91e090aca2c70599d2e6c 820 shellfu-bash-bkrh_0.0.3-1.debian.tar.xz
Checksums-Sha256:
 d2ee972e8a60dcbac78c913095ddf437d1b758f55b9b9a5adcc5f367f41b2408 34026 shellfu-bash-bkrh_0.0.3.orig.tar.gz
 437a8dc2574f31cfc769a82d58cd206cb9e04f91113804cd60719d0ddfdb902b 820 shellfu-bash-bkrh_0.0.3-1.debian.tar.xz
Files:
 2f0c07c6e4aa3b19c16edb0b91e7ac0d 34026 shellfu-bash-bkrh_0.0.3.orig.tar.gz
 2348d88a187c41b3b2fc32f5904ce2eb 820 shellfu-bash-bkrh_0.0.3-1.debian.tar.xz
