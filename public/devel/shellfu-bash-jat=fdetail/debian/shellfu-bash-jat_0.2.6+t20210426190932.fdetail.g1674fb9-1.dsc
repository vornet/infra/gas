Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.6+t20210426190932.fdetail.g1674fb9-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 1a82099eb5ddc546b9a8dc76575bcaa7004cecff 83541 shellfu-bash-jat_0.2.6+t20210426190932.fdetail.g1674fb9.orig.tar.gz
 6167de57e8b7e3f73f4ddc7e01eab3c890d51aac 1024 shellfu-bash-jat_0.2.6+t20210426190932.fdetail.g1674fb9-1.debian.tar.xz
Checksums-Sha256:
 f87c797adcc5ccc4a27dfd26dcc801689d13d55f2bfb944d53ced4ce171e1411 83541 shellfu-bash-jat_0.2.6+t20210426190932.fdetail.g1674fb9.orig.tar.gz
 32ba33fd378c586e92c76fa8757e7f774d2eb75a80635605461cde35e5fe337f 1024 shellfu-bash-jat_0.2.6+t20210426190932.fdetail.g1674fb9-1.debian.tar.xz
Files:
 7184f52f97848342692d5857cdc4a215 83541 shellfu-bash-jat_0.2.6+t20210426190932.fdetail.g1674fb9.orig.tar.gz
 af381d4e3a283ba267ad8f8b626620ab 1024 shellfu-bash-jat_0.2.6+t20210426190932.fdetail.g1674fb9-1.debian.tar.xz
