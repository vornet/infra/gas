Format: 3.0 (quilt)
Source: vdk
Binary: vdk-common, vdk-pylib, vdk-zigbin
Architecture: all
Version: 0.2.6+t20250311190104.dev.ge3864c9-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk-common deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
 vdk-zigbin deb misc extra arch=all
Checksums-Sha1:
 7b62d6531c465ada758270f9d6af2e397053e6a4 92326 vdk_0.2.6+t20250311190104.dev.ge3864c9.orig.tar.gz
 1cfff81a92ed6d6620af9139b2111279fde9a48f 1260 vdk_0.2.6+t20250311190104.dev.ge3864c9-1.debian.tar.xz
Checksums-Sha256:
 516d37a4f15b165e42f583ffdbd6b863b753d253fb9cf48c5469f08bad2fcde5 92326 vdk_0.2.6+t20250311190104.dev.ge3864c9.orig.tar.gz
 4a4fee77d4b0ee7773bb652e8dc6ade9fa5aacd19fadaee7b6e5d45bcfe83e8d 1260 vdk_0.2.6+t20250311190104.dev.ge3864c9-1.debian.tar.xz
Files:
 cf40072bd2939d42ac039779c1d2c570 92326 vdk_0.2.6+t20250311190104.dev.ge3864c9.orig.tar.gz
 087add7c9a5cdbfcb1f35dcae559dadf 1260 vdk_0.2.6+t20250311190104.dev.ge3864c9-1.debian.tar.xz
