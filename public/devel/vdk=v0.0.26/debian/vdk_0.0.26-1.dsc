Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.26-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 d374124e5bc37180a4b93599fa3cb7c24691715e 73738 vdk_0.0.26.orig.tar.gz
 d21e5f0c1e054b23409128f9e04a3e02563b00e8 1028 vdk_0.0.26-1.debian.tar.xz
Checksums-Sha256:
 222a052aebddd4649bb2cd10740093bef96e76c88c3582429306067d3c3f6422 73738 vdk_0.0.26.orig.tar.gz
 6e9b73eeb7f8027e0790bac4666cec9b4c56a965b86954d9ea9a2b456cd4072d 1028 vdk_0.0.26-1.debian.tar.xz
Files:
 c8e68a726a616546415de3ceb0ba2b82 73738 vdk_0.0.26.orig.tar.gz
 17acc8a05f9c7636f70ae25884092e91 1028 vdk_0.0.26-1.debian.tar.xz
