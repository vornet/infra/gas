Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.49-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 5101931c5d4f775220f69f67060febdfaed404fa 93099 shellfu_0.10.49.orig.tar.gz
 d6a73e3ff7ce54801b866d08049a373157aa2cc3 2228 shellfu_0.10.49-1.debian.tar.xz
Checksums-Sha256:
 c3642ac0f04ee18c1fdbfd5a2dc6d1aab5f9462fd7f79816d9eba85464d954cc 93099 shellfu_0.10.49.orig.tar.gz
 a070a2e98130aa8101bbb22ab6ecbd6c26edd8e0c4488773bca55feb2887184c 2228 shellfu_0.10.49-1.debian.tar.xz
Files:
 c4a3f4b65ddab6069266c5435b5a3fe9 93099 shellfu_0.10.49.orig.tar.gz
 303883fbc0e4c958adbaba4f75ba2af2 2228 shellfu_0.10.49-1.debian.tar.xz
