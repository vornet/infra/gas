Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.61-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 628488b2c735f4e32a26aea4dc1b3dd12dd46491 93525 shellfu_0.10.61.orig.tar.gz
 e01ec6d2d3d3d41c43a5e4c8ae9e741f327cee6a 2228 shellfu_0.10.61-1.debian.tar.xz
Checksums-Sha256:
 27772c78761e8bda5493405325a7d2824465370823955fc7d7f912454d76bfff 93525 shellfu_0.10.61.orig.tar.gz
 c64ef21e1633b5c8743b2bda14453828390746d88511c6ca21c2b8201e1a3674 2228 shellfu_0.10.61-1.debian.tar.xz
Files:
 66008cb8d1d4d064b28f82ee26b3a876 93525 shellfu_0.10.61.orig.tar.gz
 e9b9fb4f761ad8124203c64f81a0e897 2228 shellfu_0.10.61-1.debian.tar.xz
