Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.6-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 bc64e1ffdc81a51ff522cebe03977c30eb8f6f65 81219 shellfu-bash-jat_0.2.6.orig.tar.gz
 a14521a078ea321b37520d0f2cee27f67e08fe71 996 shellfu-bash-jat_0.2.6-1.debian.tar.xz
Checksums-Sha256:
 fd64cfb931445b8a8089023deedb9060349483c29b3791f5fed15e0e65960a94 81219 shellfu-bash-jat_0.2.6.orig.tar.gz
 e49df4ea300b4a81c5b4ce900a0b610cbbf0d91d0fba508d33dc2aaf08150032 996 shellfu-bash-jat_0.2.6-1.debian.tar.xz
Files:
 69597625c684975d8004e7568cfdaf75 81219 shellfu-bash-jat_0.2.6.orig.tar.gz
 c9743df75e13a8fabaffe3d8774eba54 996 shellfu-bash-jat_0.2.6-1.debian.tar.xz
