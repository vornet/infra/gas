Format: 3.0 (quilt)
Source: shellfu-bash-cached
Binary: shellfu-bash-cached
Architecture: all
Version: 0.0.7-1
Maintainer: Alois Mahdal <netvor+cached@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-cached
Build-Depends: debhelper (>= 9)
Package-List:
 shellfu-bash-cached deb misc extra arch=all
Checksums-Sha1:
 af80e3c88610e6e2c5873a8c95e25f70e1cb5363 34759 shellfu-bash-cached_0.0.7.orig.tar.gz
 61de8fe927859d7fc1817a8db0b11b1d1b3612fe 872 shellfu-bash-cached_0.0.7-1.debian.tar.xz
Checksums-Sha256:
 aed7f319e5c78ec0ec80573ec3e44fd7a9a3ef0824b26a6da1a74f741e44fe45 34759 shellfu-bash-cached_0.0.7.orig.tar.gz
 76c000b24d8a19b706d5d6b400816a1ac13dca10f566f656841bdab81f83075d 872 shellfu-bash-cached_0.0.7-1.debian.tar.xz
Files:
 fc22e9bc13e25fcfd7e25b227d1270a5 34759 shellfu-bash-cached_0.0.7.orig.tar.gz
 3d2b7ed593203cdd33cd52822191c797 872 shellfu-bash-cached_0.0.7-1.debian.tar.xz
