Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.2.0-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, python3-saturnin (>= 0.0.0), python3-saturnin (<< 0.1.0), shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 f5ed015fcc4d3bbb4e2c98a1061ab4d1cc0c0b4f 79588 bmo_0.2.0.orig.tar.gz
 a030af99bd8a2c0bd901f5e493e15d08fcf660af 1756 bmo_0.2.0-1.debian.tar.xz
Checksums-Sha256:
 95fcfae8e8d2fffd1088a4f7b02006cd33cdd232b6ef964af59e78804093000c 79588 bmo_0.2.0.orig.tar.gz
 68ac10a3a50f300377ec43764a319a43766a800cbadc4eefbe735cbc74738983 1756 bmo_0.2.0-1.debian.tar.xz
Files:
 c81034edfb9eb3765fd94c09563b8cb0 79588 bmo_0.2.0.orig.tar.gz
 01d7dbb2eaab4688949dc7c3d0b6a9de 1756 bmo_0.2.0-1.debian.tar.xz
