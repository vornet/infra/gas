Format: 3.0 (quilt)
Source: shellfu-bash-inigrep
Binary: shellfu-bash-inigrep
Architecture: all
Version: 0.11.2-1
Maintainer: Alois Mahdal <netvor+inigrep@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-inigrep
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-inigrep deb misc extra arch=all
Checksums-Sha1:
 58011d6b36f115b0e7beb4f56f1292f6f233d1b7 76620 shellfu-bash-inigrep_0.11.2.orig.tar.gz
 5b1741f269b51af1115c535160cb3263248f0244 924 shellfu-bash-inigrep_0.11.2-1.debian.tar.xz
Checksums-Sha256:
 c4631997d177c73704eac146f9d74488b72d4106d3885c4908011b9f82593d1c 76620 shellfu-bash-inigrep_0.11.2.orig.tar.gz
 2b9f5f1e94d3065b68e6b8511f8759b744b2b67a4188c452b41cf4e38e0b3d88 924 shellfu-bash-inigrep_0.11.2-1.debian.tar.xz
Files:
 3f2ce0ce62e70f00a9dc128d877b731c 76620 shellfu-bash-inigrep_0.11.2.orig.tar.gz
 521806d33fc9c49a13ec1ffac1e59277 924 shellfu-bash-inigrep_0.11.2-1.debian.tar.xz
