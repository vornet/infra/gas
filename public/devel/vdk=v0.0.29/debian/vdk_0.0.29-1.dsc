Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.29-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 e8dadc445135c9db0c7847e82d7d794cf5bf4394 75349 vdk_0.0.29.orig.tar.gz
 47d7431677ab9a00ed0bd0b0c5b0250929f7840f 1060 vdk_0.0.29-1.debian.tar.xz
Checksums-Sha256:
 8fa24129b6d909ffed916d2106fa7541742c16604f11727fd5c2239b1168c3df 75349 vdk_0.0.29.orig.tar.gz
 67d8d40fafeaae983a295ebae65138e8547bd3ebeaaac3bcb1121e1c039eb81c 1060 vdk_0.0.29-1.debian.tar.xz
Files:
 60fbe8e451fe19d869c0d5a5d637f987 75349 vdk_0.0.29.orig.tar.gz
 100a7cc248fb1e91f11d8c8895242677 1060 vdk_0.0.29-1.debian.tar.xz
