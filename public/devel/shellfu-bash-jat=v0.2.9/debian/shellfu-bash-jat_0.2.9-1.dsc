Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.9-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 742a51ed8bfca3a1c9c3a31d0e1064aa76232948 84433 shellfu-bash-jat_0.2.9.orig.tar.gz
 6d2a5711a0d3448a9afdb9c43a3672bf6ee35342 1004 shellfu-bash-jat_0.2.9-1.debian.tar.xz
Checksums-Sha256:
 a135da48236ee0cfebb83bd07f881f4c5bbd6229490f73ba92b4d409edfb7101 84433 shellfu-bash-jat_0.2.9.orig.tar.gz
 1efebd746c99bfc594484c7e52435b0598462938baa676bdeacdbe57812cfbd6 1004 shellfu-bash-jat_0.2.9-1.debian.tar.xz
Files:
 432ad143f16fb8b2fb8f5ec081a05f52 84433 shellfu-bash-jat_0.2.9.orig.tar.gz
 b61fdfd30e9e6eb7623a1f08deb623c5 1004 shellfu-bash-jat_0.2.9-1.debian.tar.xz
