Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.22-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.50), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0)
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 5eb7d0751b04a162c3ca0df21916e43729a3bdd5 72457 bmo_0.1.22.orig.tar.gz
 a33206f9349c3f9315bab3a673f5912deae7727a 1604 bmo_0.1.22-1.debian.tar.xz
Checksums-Sha256:
 0c9aa549c0c465a1c7c7453d0989924af4b3d0dc675c7826e5e35fe2f7d60740 72457 bmo_0.1.22.orig.tar.gz
 c22e0b7dff5cc13251e66b1da479da93e17fde1bdffde06896b4b9cdc3ec9afe 1604 bmo_0.1.22-1.debian.tar.xz
Files:
 ea668cdf8bbd5bf0150def65fa356bc0 72457 bmo_0.1.22.orig.tar.gz
 f27044c932a538c92a42e6a487f26939 1604 bmo_0.1.22-1.debian.tar.xz
