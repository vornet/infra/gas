Format: 3.0 (quilt)
Source: shellfu-bash-jats
Binary: shellfu-bash-jats
Architecture: all
Version: 0.0.8-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jats
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty
Package-List:
 shellfu-bash-jats deb misc extra arch=all
Checksums-Sha1:
 e62a062853723cc314f2fb583650f84e78f9c1e3 48156 shellfu-bash-jats_0.0.8.orig.tar.gz
 9b6fa948c9d47a11ae3e1f73346a99f45e33fb89 904 shellfu-bash-jats_0.0.8-1.debian.tar.xz
Checksums-Sha256:
 a1a8b476358438fccc5940e8ee63bfbb6c1abb55a7cba838256456294168f817 48156 shellfu-bash-jats_0.0.8.orig.tar.gz
 eef090962619b272dcdd5d6f270ebb8f38d2fe953fb16c26c4d19c3012b515f3 904 shellfu-bash-jats_0.0.8-1.debian.tar.xz
Files:
 d5cd8fb817c174f554120be78ce6bf06 48156 shellfu-bash-jats_0.0.8.orig.tar.gz
 99dcbb387683896ee101c255b8aed8cb 904 shellfu-bash-jats_0.0.8-1.debian.tar.xz
