Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.47+t20211109215829.ul.g21228ab-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps, util-linux
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 00ec7fe7d70c56144d1eaf466187d0f991a51ab2 85886 shellfu_0.10.47+t20211109215829.ul.g21228ab.orig.tar.gz
 80792331a10872f84c522fe3c6d0833bad8ff3ab 2236 shellfu_0.10.47+t20211109215829.ul.g21228ab-1.debian.tar.xz
Checksums-Sha256:
 9156540d0f512d14e4c6629092cffa446caafa2e5af9c8eb95165ee531bd1c10 85886 shellfu_0.10.47+t20211109215829.ul.g21228ab.orig.tar.gz
 e91c9cfa36f57542c2b4d78341294ef301dbf3a38258d0a3357f14a013eb21e2 2236 shellfu_0.10.47+t20211109215829.ul.g21228ab-1.debian.tar.xz
Files:
 77e1d1188576d79a35e5af53688b309b 85886 shellfu_0.10.47+t20211109215829.ul.g21228ab.orig.tar.gz
 8ee2be84ec7e38608387e77e545d70f2 2236 shellfu_0.10.47+t20211109215829.ul.g21228ab-1.debian.tar.xz
