Format: 3.0 (quilt)
Source: shellfu-bash-uripecker
Binary: shellfu-bash-uripecker
Architecture: all
Version: 0.1.10-1
Maintainer: Alois Mahdal <netvor+uripecker@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-uripecker
Build-Depends: debhelper (>= 9), make, python, libpython3.7-minimal, libpython3.7-stdlib, shellfu (>= 0.10.4), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-uripecker deb misc extra arch=all
Checksums-Sha1:
 d19cbed6a1e720224393a511757a878f415c0daa 46456 shellfu-bash-uripecker_0.1.10.orig.tar.gz
 d2df14f1b9e2547f0fbbecada6796fb79b63a7e4 1076 shellfu-bash-uripecker_0.1.10-1.debian.tar.xz
Checksums-Sha256:
 9a13fb287a34480afc78fcc2668d4bafa707afa8d29d2f76c81f80dc1868817a 46456 shellfu-bash-uripecker_0.1.10.orig.tar.gz
 0bea093b0ae1b4b8dac2ad21009a6023557256240566ae93a2ac4e422e8a0f37 1076 shellfu-bash-uripecker_0.1.10-1.debian.tar.xz
Files:
 9ed028f131041873b95262d1d342e9e6 46456 shellfu-bash-uripecker_0.1.10.orig.tar.gz
 45ca6c8886bf134879b52a768cb34c40 1076 shellfu-bash-uripecker_0.1.10-1.debian.tar.xz
