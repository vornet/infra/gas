Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.11-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 cb125e3bddbdea41be18c1f9656d01bcadb852bb 71130 jattool_0.3.11.orig.tar.gz
 793dc6d445ca23f78a13a006d012f4e18d027e51 1488 jattool_0.3.11-1.debian.tar.xz
Checksums-Sha256:
 31f02430c280502b439ae2b7e2892b9f34fdac5a853c8c1ac337d977881b7c8e 71130 jattool_0.3.11.orig.tar.gz
 40d2af8899dcde56768538d9af0932157e246b973b736232a0d70432533f5719 1488 jattool_0.3.11-1.debian.tar.xz
Files:
 57442fef34a00a3e9b95098d8ffbb75d 71130 jattool_0.3.11.orig.tar.gz
 835fb4bea36b76a49dc101a264154e33 1488 jattool_0.3.11-1.debian.tar.xz
