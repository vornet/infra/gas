Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.9-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 0a9b1fc750de22400cbbdc08578d3f8b2acfd14e 71198 jattool_0.3.9.orig.tar.gz
 ee8be446a40f27999f86a77b35d018fc620632d4 1492 jattool_0.3.9-1.debian.tar.xz
Checksums-Sha256:
 9226036a5bff4ff4241a812e430ec5bb392ce7baeb1128464ea5f53e0ed2d047 71198 jattool_0.3.9.orig.tar.gz
 a77c881684cdd0bd1430dd7bb5865e4eeb74abc6127a90bf20d2369509ccec65 1492 jattool_0.3.9-1.debian.tar.xz
Files:
 9675928ac0e7171ba54604270de61903 71198 jattool_0.3.9.orig.tar.gz
 1e1a2a4f002a3673a603139b90e3baa1 1492 jattool_0.3.9-1.debian.tar.xz
