Format: 3.0 (quilt)
Source: shellfu-bash-uuevid
Binary: shellfu-bash-uuevid
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <netvor+uuevid@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-uuevid
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-uuevid deb misc extra arch=all
Checksums-Sha1:
 198c0463c73bbfe941fa0b6359ce9e59dbb9b2b9 33939 shellfu-bash-uuevid_0.0.5.orig.tar.gz
 d1cd0d8c65c3934b707c157eb53d904b4d80a5e9 884 shellfu-bash-uuevid_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 5e8295c75f42cead4034dd035b38f0a67f65c2d69ac85f57a06291fd9c7d2bca 33939 shellfu-bash-uuevid_0.0.5.orig.tar.gz
 67f6f8f9c62a749e5de3cd6deccf1cf9f6ab04fde26bdccd1eadb9030f4a3c74 884 shellfu-bash-uuevid_0.0.5-1.debian.tar.xz
Files:
 b1a2b45e82cc93a5f13a3800251be4ea 33939 shellfu-bash-uuevid_0.0.5.orig.tar.gz
 84063cc9d26a373ea03ebe4e7245cbf1 884 shellfu-bash-uuevid_0.0.5-1.debian.tar.xz
