Format: 3.0 (quilt)
Source: shellfu-bash-uripecker
Binary: shellfu-bash-uripecker
Architecture: all
Version: 0.1.12-1
Maintainer: Alois Mahdal <netvor+uripecker@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-uripecker
Build-Depends: debhelper (>= 9), make, python, libpython3.7-minimal, libpython3.7-stdlib, shellfu (>= 0.10.4), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-pretty
Package-List:
 shellfu-bash-uripecker deb misc extra arch=all
Checksums-Sha1:
 8266d1440fe0b66f3dce72c252b7eb717575145d 47592 shellfu-bash-uripecker_0.1.12.orig.tar.gz
 601e6ac23456dad3d3c80f107f63c341c69dffa6 1076 shellfu-bash-uripecker_0.1.12-1.debian.tar.xz
Checksums-Sha256:
 f34bdde788ccd57be0b2052b21030e3c0c0c6106f19e397fd91ecd827a8f17fe 47592 shellfu-bash-uripecker_0.1.12.orig.tar.gz
 20e886267c3d797cb01b8417aa3511268bf2d8cc684835ebfc5aa03d1422f054 1076 shellfu-bash-uripecker_0.1.12-1.debian.tar.xz
Files:
 01b2d59532fb2e06996b574dc04ae6aa 47592 shellfu-bash-uripecker_0.1.12.orig.tar.gz
 fcf6962283b22f82a517252be37fad15 1076 shellfu-bash-uripecker_0.1.12-1.debian.tar.xz
