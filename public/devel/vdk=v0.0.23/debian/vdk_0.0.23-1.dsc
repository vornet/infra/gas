Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.23-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 953735300d5b3c3e99573cb3e3007ff816188fbc 70701 vdk_0.0.23.orig.tar.gz
 c186f7d77db6aebc1ea4f198a7a25a89532ad7d9 1024 vdk_0.0.23-1.debian.tar.xz
Checksums-Sha256:
 5e621840812898e98b1f0f1a0201655874f28f488359d7bda49cbcd1abaed559 70701 vdk_0.0.23.orig.tar.gz
 7d89f06c2ed4b2bc0f2c7b6e99cc8cfc61755d676c3b5cafe72b2054fe8b4cf8 1024 vdk_0.0.23-1.debian.tar.xz
Files:
 c808c4ba29e1774c15554327af566382 70701 vdk_0.0.23.orig.tar.gz
 4a2daa04f997c4247b4179ba47214bcb 1024 vdk_0.0.23-1.debian.tar.xz
