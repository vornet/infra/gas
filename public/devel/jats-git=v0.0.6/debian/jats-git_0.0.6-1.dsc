Format: 3.0 (quilt)
Source: jats-git
Binary: jats-git
Architecture: all
Version: 0.0.6-1
Maintainer: Alois Mahdal <amahdal@redhat.com>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-git deb misc extra arch=all
Checksums-Sha1:
 78eae5870f73791522d3601c12758788c3d04850 2118 jats-git_0.0.6.orig.tar.gz
 8326828196718411d10c5e42a81f43fca2516023 784 jats-git_0.0.6-1.debian.tar.xz
Checksums-Sha256:
 1daeaf05e3fa9b28bee922208b05f928f09b71043a08c31e0a67fa8481b36c54 2118 jats-git_0.0.6.orig.tar.gz
 5c848f5b0fc6728b6593c905aafe33dd6cafc056670086def71a687b4608c46d 784 jats-git_0.0.6-1.debian.tar.xz
Files:
 59336d858d6a824b550e73538ea10dce 2118 jats-git_0.0.6.orig.tar.gz
 fe409a9c1f76cf10dfb830c33e0f89dd 784 jats-git_0.0.6-1.debian.tar.xz
