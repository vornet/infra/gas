Format: 3.0 (quilt)
Source: shellfu-bash-saturnin
Binary: shellfu-bash-saturnin
Architecture: all
Version: 0.5.12-1
Maintainer: Alois Mahdal <netvor+saturnin@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu-bash-saturnin
Build-Depends: debhelper (>= 9), make, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-inigrep, shellfu-bash-pretty, shellfu-sh-exit
Package-List:
 shellfu-bash-saturnin deb misc extra arch=all
Checksums-Sha1:
 e312f3f96f8a2fb5e7ac43c3d2d5557686ba322b 57848 shellfu-bash-saturnin_0.5.12.orig.tar.gz
 99cc11db7daad53271e0f472571ea65fff11a618 1096 shellfu-bash-saturnin_0.5.12-1.debian.tar.xz
Checksums-Sha256:
 2e500f04a156cd33d3e9fa18bb7f3d048f3ded03a23ea422d1cfea7dbec7a333 57848 shellfu-bash-saturnin_0.5.12.orig.tar.gz
 6f19ddfb59de45bf97efa268a2f38c032a918cdf8b219d0c585870b338014da4 1096 shellfu-bash-saturnin_0.5.12-1.debian.tar.xz
Files:
 05da5f2efa8f71bcb59c71fab4488398 57848 shellfu-bash-saturnin_0.5.12.orig.tar.gz
 2fcfbd87bb066fd688b9157e814c1b2c 1096 shellfu-bash-saturnin_0.5.12-1.debian.tar.xz
