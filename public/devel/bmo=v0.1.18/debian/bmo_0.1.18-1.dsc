Format: 3.0 (quilt)
Source: bmo
Binary: bmo, shellfu-bash-bmo, python3-bmo
Architecture: all
Version: 0.1.18-1
Maintainer: Alois Mahdal <netvor+bmo@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/bmo/bmo
Build-Depends: debhelper (>= 9), shellfu (>= 0.10.11), shellfu (<< 0.11.0), shellfu-bash-pretty, shellfu-bash-saturnin (>= 0.5.6), shellfu-bash-saturnin (<< 0.6.0), xclip
Package-List:
 bmo deb misc extra arch=all
 python3-bmo deb misc extra arch=all
 shellfu-bash-bmo deb misc extra arch=all
Checksums-Sha1:
 c8ab3546e0674fdd2ea145af3dc4e1950ccbc730 69163 bmo_0.1.18.orig.tar.gz
 77a0ff566af4c407160873006ff1a02fb513447b 1604 bmo_0.1.18-1.debian.tar.xz
Checksums-Sha256:
 f0e7ddeea1e2eb1193c17d6ec9bbeab8282248d4b6fd11535e7e32447483813c 69163 bmo_0.1.18.orig.tar.gz
 da5541f04eec747d8ccb33849dcdef8db71f38e804bdc08026cfb690c3e793c7 1604 bmo_0.1.18-1.debian.tar.xz
Files:
 c5863031cf1891015fe2ff43ee1a6a18 69163 bmo_0.1.18.orig.tar.gz
 cd46fc8351f318a7d0d8851f637705c5 1604 bmo_0.1.18-1.debian.tar.xz
