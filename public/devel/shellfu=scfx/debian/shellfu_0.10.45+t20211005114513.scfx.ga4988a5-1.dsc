Format: 3.0 (quilt)
Source: shellfu
Binary: shellfu, shellfu-bash, shellfu-bash-arr, shellfu-bash-pretty, shellfu-bash-sfdoc, shellfu-bash-sfpi, shellfu-devel, shellfu-sh, shellfu-sh-coerce, shellfu-sh-exit, shellfu-sh-isa, shellfu-sh-termcolors
Architecture: all
Version: 0.10.45+t20211005114513.scfx.ga4988a5-1
Maintainer: Alois Mahdal <netvor+shellfu@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/shellfu/shellfu
Build-Depends: debhelper (>= 9), make, perl, procps
Package-List:
 shellfu deb misc extra arch=all
 shellfu-bash deb misc extra arch=all
 shellfu-bash-arr deb misc extra arch=all
 shellfu-bash-pretty deb misc extra arch=all
 shellfu-bash-sfdoc deb misc extra arch=all
 shellfu-bash-sfpi deb misc extra arch=all
 shellfu-devel deb misc extra arch=all
 shellfu-sh deb misc extra arch=all
 shellfu-sh-coerce deb misc extra arch=all
 shellfu-sh-exit deb misc extra arch=all
 shellfu-sh-isa deb misc extra arch=all
 shellfu-sh-termcolors deb misc extra arch=all
Checksums-Sha1:
 01d3b5f184e40137ab5f1cba9088f48385195b36 85078 shellfu_0.10.45+t20211005114513.scfx.ga4988a5.orig.tar.gz
 916a86e76c03e344f721662e4ba5eb519aa9257e 2228 shellfu_0.10.45+t20211005114513.scfx.ga4988a5-1.debian.tar.xz
Checksums-Sha256:
 03b56a3bd70fafd3b5d93a14fe5aa564a553aeb4c4002caef3b564ce90f9b3f4 85078 shellfu_0.10.45+t20211005114513.scfx.ga4988a5.orig.tar.gz
 3cb0d9a0b5602d7033ebaac4f9ea84a09b620858d08bdef72658067685d09d7e 2228 shellfu_0.10.45+t20211005114513.scfx.ga4988a5-1.debian.tar.xz
Files:
 29c53958c2b4c8666beb889eb6e5b38c 85078 shellfu_0.10.45+t20211005114513.scfx.ga4988a5.orig.tar.gz
 5cff424f50e8159648a20b4eef3ed727 2228 shellfu_0.10.45+t20211005114513.scfx.ga4988a5-1.debian.tar.xz
