Format: 3.0 (quilt)
Source: jats-python-clapp
Binary: jats-python-clapp
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+clapp@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-python-clapp deb misc extra arch=all
Checksums-Sha1:
 0d0a58e66fb02ccf743e515edce6c65fbb3adbfb 3018 jats-python-clapp_0.0.2.orig.tar.gz
 d3a80654076238669603cab86027ad66c53fd5ba 848 jats-python-clapp_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 553344a542525ef60232cac1a03deb3e1f9656a878a7874afc1150b6a1a9a30e 3018 jats-python-clapp_0.0.2.orig.tar.gz
 2e47b1cdcbbefd3adfc67acd2fdeb8a833df38f17db1034aa506268d7b8d352b 848 jats-python-clapp_0.0.2-1.debian.tar.xz
Files:
 3f42b5a4fe73c513abd718e4bba524e3 3018 jats-python-clapp_0.0.2.orig.tar.gz
 486147508c7de1b1d64d50ac85a399b8 848 jats-python-clapp_0.0.2-1.debian.tar.xz
