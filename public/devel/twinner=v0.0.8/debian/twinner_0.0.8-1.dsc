Format: 3.0 (quilt)
Source: twinner
Binary: twinner
Architecture: all
Version: 0.0.8-1
Maintainer: Alois Mahdal <netvor+twinner@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://pagure.io/twinner
Build-Depends: debhelper (>= 9), make
Package-List:
 twinner deb misc extra arch=all
Checksums-Sha1:
 8d5941177e0b3a3541ceb0044467d9954036f42a 34061 twinner_0.0.8.orig.tar.gz
 80cda58dcf41a81973cefbf4070138395c9e5ec0 804 twinner_0.0.8-1.debian.tar.xz
Checksums-Sha256:
 532789bb3289e54a02b5cbbd1682bee00834eb6891d1f5526ce01a4f2311f2f7 34061 twinner_0.0.8.orig.tar.gz
 5e23c817c09acd0e834e4fcd6f1970ac89fd1bdecca4cabb9263052914d59c56 804 twinner_0.0.8-1.debian.tar.xz
Files:
 7b3d80657f1b958761eccc8b0f57ec5c 34061 twinner_0.0.8.orig.tar.gz
 237ead27b4921b6677cc694530cc684b 804 twinner_0.0.8-1.debian.tar.xz
