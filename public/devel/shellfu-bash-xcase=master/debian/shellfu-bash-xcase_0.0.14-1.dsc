Format: 3.0 (quilt)
Source: shellfu-bash-xcase
Binary: shellfu-bash-xcase
Architecture: all
Version: 0.0.14-1
Maintainer: Alois Mahdal <netvor+xcase@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-xcase
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-xcase deb misc extra arch=all
Checksums-Sha1:
 7d148094d54b92c9f9f75956858bad30facba7de 46568 shellfu-bash-xcase_0.0.14.orig.tar.gz
 72d0391a2132cdeccee75c3ff66ff351a6c810a9 1016 shellfu-bash-xcase_0.0.14-1.debian.tar.xz
Checksums-Sha256:
 18a429297244cbf482dd2550ec9644695cac3291b97f0214c056e7d18987881c 46568 shellfu-bash-xcase_0.0.14.orig.tar.gz
 f6d7592f147a3b44597ee68d139486137a11cb7473d602cccf3808c6a8103f1d 1016 shellfu-bash-xcase_0.0.14-1.debian.tar.xz
Files:
 0e9ccdc23bf8d7886821a6fe2c2d0923 46568 shellfu-bash-xcase_0.0.14.orig.tar.gz
 03310c4a2ef9bd7b296e9593f0d285e4 1016 shellfu-bash-xcase_0.0.14-1.debian.tar.xz
