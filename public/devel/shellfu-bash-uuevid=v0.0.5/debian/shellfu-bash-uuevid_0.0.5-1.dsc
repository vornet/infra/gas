Format: 3.0 (quilt)
Source: shellfu-bash-uuevid
Binary: shellfu-bash-uuevid
Architecture: all
Version: 0.0.5-1
Maintainer: Alois Mahdal <netvor+uuevid@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-uuevid
Build-Depends: debhelper (>= 9), make
Package-List:
 shellfu-bash-uuevid deb misc extra arch=all
Checksums-Sha1:
 11a1c0d770247c60de787e2e8ed63a081ceff351 33938 shellfu-bash-uuevid_0.0.5.orig.tar.gz
 d1cd0d8c65c3934b707c157eb53d904b4d80a5e9 884 shellfu-bash-uuevid_0.0.5-1.debian.tar.xz
Checksums-Sha256:
 4db940b52259d2af98f0af9ec606148ef722f753c77b028c1680ce88be26e454 33938 shellfu-bash-uuevid_0.0.5.orig.tar.gz
 67f6f8f9c62a749e5de3cd6deccf1cf9f6ab04fde26bdccd1eadb9030f4a3c74 884 shellfu-bash-uuevid_0.0.5-1.debian.tar.xz
Files:
 592381c4d33c879a8d925ce0cd401e83 33938 shellfu-bash-uuevid_0.0.5.orig.tar.gz
 84063cc9d26a373ea03ebe4e7245cbf1 884 shellfu-bash-uuevid_0.0.5-1.debian.tar.xz
