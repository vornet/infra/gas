Format: 3.0 (quilt)
Source: shellfu-bash-jat
Binary: shellfu-bash-jat
Architecture: all
Version: 0.2.12-1
Maintainer: Alois Mahdal <netvor+jats@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/shellfu-bash-jat
Build-Depends: bsdmainutils, debhelper (>= 9), make, perl, shellfu (>= 0.10.40), shellfu (<< 0.11.0), shellfu-bash, shellfu-bash-jats, shellfu-bash-pretty, shellfu-bash-sfpi, shellfu-sh-isa, shellfu-sh-termcolors, tar
Package-List:
 shellfu-bash-jat deb misc extra arch=all
Checksums-Sha1:
 44d7173306e58a328ccb9ce2ac847d0444e36008 101658 shellfu-bash-jat_0.2.12.orig.tar.gz
 84df44166cc0e0e0cb029a105f435d0296391cb2 1004 shellfu-bash-jat_0.2.12-1.debian.tar.xz
Checksums-Sha256:
 0ab6feb153973545652eba8ed205aa907b467a52699a9beb214df79eb06032da 101658 shellfu-bash-jat_0.2.12.orig.tar.gz
 f19021dfcd1087be86982a446445dff73533503def4d9854ade7ebb73c522d70 1004 shellfu-bash-jat_0.2.12-1.debian.tar.xz
Files:
 3676d579e26f3efe3b5a512d8b34fd34 101658 shellfu-bash-jat_0.2.12.orig.tar.gz
 63ee685edb553e0e8487faf2f4b5cece 1004 shellfu-bash-jat_0.2.12-1.debian.tar.xz
