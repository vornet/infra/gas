Format: 3.0 (quilt)
Source: jats-python-clapp
Binary: jats-python-clapp
Architecture: all
Version: 0.0.2-1
Maintainer: Alois Mahdal <netvor+clapp@vornet.cz>
Standards-Version: 3.9.2
Build-Depends: jattool-tdk (>= 0.3.0), jattool-tdk (<< 0.4.0), debhelper (>= 9)
Package-List:
 jats-python-clapp deb misc extra arch=all
Checksums-Sha1:
 af7b598c77e69e3fe70ca3d3a004b3b952ad50d4 3015 jats-python-clapp_0.0.2.orig.tar.gz
 d3a80654076238669603cab86027ad66c53fd5ba 848 jats-python-clapp_0.0.2-1.debian.tar.xz
Checksums-Sha256:
 1d743f423bb64c3e5ac59f3980d88fa28f74ddad6aa89bb888792192eb2a70bf 3015 jats-python-clapp_0.0.2.orig.tar.gz
 2e47b1cdcbbefd3adfc67acd2fdeb8a833df38f17db1034aa506268d7b8d352b 848 jats-python-clapp_0.0.2-1.debian.tar.xz
Files:
 38953d5771c713bdc119cc308a613249 3015 jats-python-clapp_0.0.2.orig.tar.gz
 486147508c7de1b1d64d50ac85a399b8 848 jats-python-clapp_0.0.2-1.debian.tar.xz
