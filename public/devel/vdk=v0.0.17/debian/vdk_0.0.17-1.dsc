Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.17-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 c7d0fe07a0e7075782490118615e62444e48f2a9 66119 vdk_0.0.17.orig.tar.gz
 89b4dde2408735d117571a4330dd0b88ed4afc4f 1012 vdk_0.0.17-1.debian.tar.xz
Checksums-Sha256:
 d7706776169567af0e9c5001adb13fc8d0f63ff3bb8660c47cabed3c3a53ebff 66119 vdk_0.0.17.orig.tar.gz
 e00eea6818f3a3e176948a525b513eb875c8a44ba13de6070f65556280fbb52b 1012 vdk_0.0.17-1.debian.tar.xz
Files:
 9227cb7036f5640b18a56dddd2e2d1c1 66119 vdk_0.0.17.orig.tar.gz
 0e7418d297f481324624728dd10e8f99 1012 vdk_0.0.17-1.debian.tar.xz
