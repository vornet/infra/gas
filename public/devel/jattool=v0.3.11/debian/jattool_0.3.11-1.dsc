Format: 3.0 (quilt)
Source: jattool
Binary: jattool, jattool-minimal, jattool-report, jattool-tdk
Architecture: all
Version: 0.3.11-1
Maintainer: Alois Mahdal <netvor+jattool@vornet.cz>
Standards-Version: 3.9.2
Vcs-Browser: https://gitlab.com/vornet/jats/jattool
Build-Depends: debhelper (>= 9), make
Package-List:
 jattool deb misc extra arch=all
 jattool-minimal deb misc extra arch=all
 jattool-report deb misc extra arch=all
 jattool-tdk deb misc extra arch=all
Checksums-Sha1:
 f7886db2c9ed73b71ae38dbcf89f55441b060512 71137 jattool_0.3.11.orig.tar.gz
 793dc6d445ca23f78a13a006d012f4e18d027e51 1488 jattool_0.3.11-1.debian.tar.xz
Checksums-Sha256:
 be4649c9a0b9a564718afbb524baeff6f88966bfd11d6fdd92d51d47e82a6404 71137 jattool_0.3.11.orig.tar.gz
 40d2af8899dcde56768538d9af0932157e246b973b736232a0d70432533f5719 1488 jattool_0.3.11-1.debian.tar.xz
Files:
 9869cd82054c300569ec348e8aec2775 71137 jattool_0.3.11.orig.tar.gz
 835fb4bea36b76a49dc101a264154e33 1488 jattool_0.3.11-1.debian.tar.xz
