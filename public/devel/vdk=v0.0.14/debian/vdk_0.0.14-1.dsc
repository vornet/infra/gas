Format: 3.0 (quilt)
Source: vdk
Binary: vdk, vdk-pylib
Architecture: all
Version: 0.0.14-1
Maintainer: Alois Mahdal <netvor+vdk@vornet.cz>
Standards-Version: 4.5.1
Vcs-Browser: https://gitlab.com/vornet/infra/vdk
Build-Depends: debhelper-compat (= 13), make
Package-List:
 vdk deb misc extra arch=all
 vdk-pylib deb misc extra arch=all
Checksums-Sha1:
 7b568276dab4c6689a3ee9b03a2b3aa19ccb7e03 62821 vdk_0.0.14.orig.tar.gz
 84b9d05fae87d821c9dc442664a5920fef4ffb78 972 vdk_0.0.14-1.debian.tar.xz
Checksums-Sha256:
 df017009e1e44bdde71b30ad7814137053537354b9ae4d1bc533eb7115e2229a 62821 vdk_0.0.14.orig.tar.gz
 dc07ec71ebcfea2b51fd000500f6fcf8c9e580cb49e9c0cea8848243288fbe73 972 vdk_0.0.14-1.debian.tar.xz
Files:
 7967a2dc4606efe64d4e7c1741539501 62821 vdk_0.0.14.orig.tar.gz
 62ca8c1e02ba937dd7fe7dd9a198d749 972 vdk_0.0.14-1.debian.tar.xz
